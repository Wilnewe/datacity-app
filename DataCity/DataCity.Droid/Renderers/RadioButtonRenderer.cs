using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using DataCity.Controls;
using DataCity.Droid.Renderers;
using Xamarin.Forms;
using Android.Content.Res;

[assembly: ExportRenderer(typeof(CustomRadioButton), typeof(RadioButtonRenderer))]
namespace DataCity.Droid.Renderers
{
    public class RadioButtonRenderer : ViewRenderer<CustomRadioButton, RadioButton>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<CustomRadioButton> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                e.OldElement.PropertyChanged += ElementOnPropertyChanged;
            }

            if (this.Control == null)
            {
                var radButton = new RadioButton(Context);
                radButton.CheckedChange += radButton_CheckedChange;
                
                //var myList = (Resources.GetColorStateList(Resource.Drawable.mycolors));
                //radButton.ButtonTintList = myList;

                int[][] states = new int[][] { new int[] { Android.Resource.Attribute.StateChecked }, new int[] { } };
                int[] colors = new int[] { Color.FromHex("#0BA3E9").ToAndroid(), Color.FromHex("#0BA3E9").ToAndroid() };
                var myList = new ColorStateList(states, colors);
                radButton.ButtonTintList = myList;

                SetNativeControl(radButton);
            }

            Control.Text = e.NewElement.Text;
            Control.Checked = e.NewElement.Checked;
            Control.SetTextColor(Xamarin.Forms.Color.FromHex("#009FE8").ToAndroid()); //e.NewElement.TextColor.ToAndroid()


            Element.PropertyChanged += ElementOnPropertyChanged;
        }

        void radButton_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            this.Element.Checked = e.IsChecked;
        }



        void ElementOnPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Checked":
                    Control.Checked = Element.Checked;
                    break;
                case "Text":
                    Control.Text = Element.Text;
                    break;

            }
        }
    }
}