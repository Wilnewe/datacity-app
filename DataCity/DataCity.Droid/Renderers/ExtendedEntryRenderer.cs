using System;
using Android.Content;
using Android.Views;
using Color = Xamarin.Forms.Color;
using Xamarin.Forms;
using DataCity.Droid.Renderers;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Text.Method;
using System.ComponentModel;
using Android.Graphics.Drawables.Shapes;
using Android.Text;
using Android.Content.Res;
using Android.OS;
using DataCity.Controls;

[assembly: ExportRenderer(typeof(ExtendedEntry), typeof(ExtendedEntryRenderer))]
namespace DataCity.Droid.Renderers
{
    public class ExtendedEntryRenderer : EntryRenderer
    {
        private const int MinDistance = 10;
        private float downX, downY, upX, upY;
        private Drawable originalBackground;

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            var view = (ExtendedEntry)Element;

            if (Control != null && e.NewElement != null && e.NewElement.IsPassword)
            {
                Control.SetTypeface(Typeface.Default, TypefaceStyle.Normal);
                Control.TransformationMethod = new PasswordTransformationMethod();
            }

            if (Control != null)
            {
                //nebude zobrazovat napovedu pri pisani
                Control.InputType = InputTypes.TextFlagNoSuggestions;
                //Control.LineHeight;
                //Control.Height;
                //Control.MaxHeight;
                //Control.MeasuredHeight;
                //Control.MinHeight;
                //Control.MinimumHeight;
                // hack ako zmenit default bottom line color
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                    Control.BackgroundTintList = ColorStateList.ValueOf(Android.Graphics.Color.White);
                else
                    Control.Background.SetColorFilter(Android.Graphics.Color.White, PorterDuff.Mode.SrcAtop);

                Drawable dr = null;
                if (e.NewElement.IsPassword)
                {
                    dr = Resources.GetDrawable(Resource.Drawable.locked);
                }
                else {

                    if (e.NewElement.Keyboard == Keyboard.Email)
                    {
                        dr = Resources.GetDrawable(Resource.Drawable.email);
                    }
                    else if (e.NewElement.Keyboard == Keyboard.Telephone)
                    {
                        dr = Resources.GetDrawable(Resource.Drawable.phone);
                    }
                    else
                    {
                        dr = Resources.GetDrawable(Resource.Drawable.user2);
                    }
                }
                Bitmap bitmap = ((BitmapDrawable)dr).Bitmap;
                //Drawable d = new BitmapDrawable(Resources, Bitmap.CreateScaledBitmap(bitmap, 20, 30, true));
                Drawable d = new BitmapDrawable(Resources, Bitmap.CreateScaledBitmap(bitmap, (int)(Control.LineHeight * 0.7), (int)(Control.LineHeight * 0.7), false));
                Control.SetCompoundDrawablesRelativeWithIntrinsicBounds(d, null, null, null);
                //iOS: Control.LeftView.BackgroundColor = UIColor.FromPatternImage(UIImage.FromFile("notes.png"));

                /*
                Control.SpellCheckingType = UITextSpellCheckingType.No;             // No Spellchecking
                Control.AutocorrectionType = UITextAutocorrectionType.No;           // No Autocorrection
                Control.AutocapitalizationType = UITextAutocapitalizationType.None; // No Autocapitalization*/
            }

            if (originalBackground == null)
                originalBackground = Control.Background;

            SetFont(view);
            SetTextAlignment(view);
            SetBorder(view);
            SetPlaceholderTextColor(view);
            SetMaxLength(view);

            if (e.NewElement == null)
            {
                this.Touch -= HandleTouch;
            }

            if (e.OldElement == null)
            {
                this.Touch += HandleTouch;
            }
        }

        void HandleTouch(object sender, TouchEventArgs e)
        {
            var element = (ExtendedEntry)this.Element;
            switch (e.Event.Action)
            {
                case MotionEventActions.Down:
                    this.downX = e.Event.GetX();
                    this.downY = e.Event.GetY();
                    return;
                case MotionEventActions.Up:
                case MotionEventActions.Cancel:
                case MotionEventActions.Move:
                    this.upX = e.Event.GetX();
                    this.upY = e.Event.GetY();

                    float deltaX = this.downX - this.upX;
                    float deltaY = this.downY - this.upY;

                    // swipe horizontal?
                    /*
                    if (Math.Abs(deltaX) > Math.Abs(deltaY))
                    {
                        if (Math.Abs(deltaX) > MinDistance)
                        {
                            if (deltaX < 0)
                            {
                                element.OnRightSwipe(this, EventArgs.Empty);
                                return;
                            }

                            if (deltaX > 0)
                            {
                                element.OnLeftSwipe(this, EventArgs.Empty);
                                return;
                            }
                        }
                        else
                        {
                            return; // We don't consume the event
                        }
                    }*/
                    return;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var view = (ExtendedEntry)Element;

            if (e.PropertyName == ExtendedEntry.FontProperty.PropertyName)
            {
                SetFont(view);
            }
            else if (e.PropertyName == ExtendedEntry.XAlignProperty.PropertyName)
            {
                SetTextAlignment(view);
            }
            else if (e.PropertyName == ExtendedEntry.HasBorderProperty.PropertyName)
            {
                //return;   
            }
            else if (e.PropertyName == ExtendedEntry.PlaceholderTextColorProperty.PropertyName)
            {
                SetPlaceholderTextColor(view);
            }
            else
            {
                base.OnElementPropertyChanged(sender, e);
                if (e.PropertyName == VisualElement.BackgroundColorProperty.PropertyName)
                {
                    this.Control.SetBackgroundColor(view.BackgroundColor.ToAndroid());
                }
            }
        }

        private void SetBorder(ExtendedEntry view)
        {
            if (view.HasBorder == false)
            {
                var shape = new ShapeDrawable(new RectShape());
                shape.Paint.Alpha = 0;
                shape.Paint.SetStyle(Paint.Style.Stroke);
                Control.SetBackgroundDrawable(shape);
                
            }
            else
            {
                /*
                var rect = new RectShape();
                var shape = new ShapeDrawable(rect);
                shape.Paint.SetStyle(Paint.Style.Stroke);
                shape.Paint.StrokeWidth = 4;
                shape.Paint.Color = Android.Graphics.Color.White;
                shape.SetPadding(new Rect(15, 15, 15, 15));
                Control.SetBackgroundDrawable(shape);*/

                Control.SetBackground(originalBackground);
            }
        }

        private void SetTextAlignment(ExtendedEntry view)
        {
            switch (view.XAlign)
            {
                case Xamarin.Forms.TextAlignment.Center:
                    Control.Gravity = GravityFlags.CenterHorizontal;
                    break;
                case Xamarin.Forms.TextAlignment.End:
                    Control.Gravity = GravityFlags.End;
                    break;
                case Xamarin.Forms.TextAlignment.Start:
                    Control.Gravity = GravityFlags.Start;
                    break;
            }
        }

        private void SetFont(ExtendedEntry view)
        {
            if (view.Font != Font.Default)
            {
                Control.TextSize = view.Font.ToScaledPixel();
                //Control.Typeface = view.Font.ToExtendedTypeface(Context);
            }
        }

        private void SetPlaceholderTextColor(ExtendedEntry view)
        {
            if (view.PlaceholderTextColor != Color.Default)
            {
                Control.SetHintTextColor(view.PlaceholderTextColor.ToAndroid());
            }

            if (!view.IsEnabled)
            {
                Control.SetTextColor(Android.Graphics.Color.Gray);
            }
        }

        private void SetMaxLength(ExtendedEntry view)
        {
            Control.SetFilters(new IInputFilter[] { new InputFilterLengthFilter(view.MaxLength) });
        }
    }
}