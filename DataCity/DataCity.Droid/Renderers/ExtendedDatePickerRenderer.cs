using System;
using Android.Content;
using Android.Views;
using Color = Xamarin.Forms.Color;
using Xamarin.Forms;
using DataCity.Droid.Renderers;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Text.Method;
using System.ComponentModel;
using Android.Graphics.Drawables.Shapes;
using Android.Text;
using Android.Content.Res;
using Android.OS;
using DataCity.Controls;

[assembly: ExportRenderer(typeof(ExtendedDatePicker), typeof(ExtendedDatePickerRenderer))]
namespace DataCity.Droid.Renderers
{
    public class ExtendedDatePickerRenderer : DatePickerRenderer
    {
        private const int MinDistance = 10;


        public ExtendedDatePickerRenderer()
        {
            
        }
        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);

            var view = (ExtendedDatePicker)Element;

            if (Control != null)
            {
                this.Control.Text = view.Placeholder;

                // hack ako zmenit default bottom line color
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                    Control.BackgroundTintList = ColorStateList.ValueOf(Android.Graphics.Color.White);
                else
                    Control.Background.SetColorFilter(Android.Graphics.Color.White, PorterDuff.Mode.SrcAtop);

                Drawable dr = Resources.GetDrawable(Resource.Drawable.calendar);
                Bitmap bitmap = ((BitmapDrawable)dr).Bitmap;
                Drawable d = new BitmapDrawable(Resources, Bitmap.CreateScaledBitmap(bitmap, (int)(Control.LineHeight * 0.7), (int)(Control.LineHeight * 0.7), false));

                Control.SetCompoundDrawablesRelativeWithIntrinsicBounds(d, null, null, null);

                SetTextAlignment(view);
                SetPlaceholderTextColor(view);
              }
        }

        

        private void SetTextAlignment(ExtendedDatePicker view)
        {
           
            Control.Gravity = GravityFlags.CenterHorizontal;
        }

       

        private void SetPlaceholderTextColor(ExtendedDatePicker view)
        {
            if (view.TextColor != Color.Default)
            {
                Control.SetHintTextColor(view.TextColor.ToAndroid());
            }
        }

       
    }
}