using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using DataCity.Controls;
using DataCity.Droid.Renderers;
using Xamarin.Forms.Platform.Android;
using System.ComponentModel;
using Android.Graphics;

[assembly: ExportRenderer(typeof(ExtendedButton), typeof(ExtendedButtonRenderer))]
namespace DataCity.Droid.Renderers
{
    public class ExtendedButtonRenderer : ButtonRenderer
    {
        protected override void OnDraw(Android.Graphics.Canvas canvas)
        {
            
            //RectF rect = new RectF(0, 0, this.Width, this.Height);
            //Path clipPath = new Path();
            //clipPath.AddRoundRect(rect, 10, 10, Path.Direction.Cw);
            //canvas.ClipPath(clipPath);
            
            base.OnDraw(canvas);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {


            }
        }
    }
}