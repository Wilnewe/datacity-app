using System;
using Android.Content;
using Android.Views;
using Color = Xamarin.Forms.Color;
using Xamarin.Forms;
using DataCity.Controls;
using DataCity.Droid.Renderers;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Text.Method;
using System.ComponentModel;
using Android.Graphics.Drawables.Shapes;
using Android.Text;
using Android.Content.Res;
using Android.OS;

[assembly: ExportRenderer(typeof(ExtendedPicker), typeof(ExtendedPickerRenderer))]
namespace DataCity.Droid.Renderers
{
    public class ExtendedPickerRenderer : PickerRenderer
    {
        private const int MinDistance = 10;
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            var view = (ExtendedPicker)Element;

            if (Control != null)
            {
                // hack ako zmenit default bottom line color
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
                    Control.BackgroundTintList = ColorStateList.ValueOf(Android.Graphics.Color.White);
                else
                    Control.Background.SetColorFilter(Android.Graphics.Color.White, PorterDuff.Mode.SrcAtop);

                Drawable dr;
                if (view.StyleId == "Pohlavie")
                {
                    dr = Resources.GetDrawable(Resource.Drawable.gender);

                }
                else
                {
                    dr = Resources.GetDrawable(Resource.Drawable.china);
                }
                Bitmap bitmap = ((BitmapDrawable)dr).Bitmap;
                Drawable d = new BitmapDrawable(Resources, Bitmap.CreateScaledBitmap(bitmap, (int)(Control.LineHeight * 0.7), (int)(Control.LineHeight * 0.7), false));

                Control.SetCompoundDrawablesRelativeWithIntrinsicBounds(d, null, null, null);

                Control.InputType = InputTypes.TextFlagNoSuggestions;

                SetTextAlignment(view);
                SetPlaceholderTextColor(view);
              }
        }

        

        private void SetTextAlignment(ExtendedPicker view)
        {
           
            Control.Gravity = GravityFlags.CenterHorizontal;
        }

       

        private void SetPlaceholderTextColor(ExtendedPicker view)
        {
            if (view.TextColor != Color.Default)
            {
                Control.SetHintTextColor(view.TextColor.ToAndroid());
            }
        }

       
    }
}