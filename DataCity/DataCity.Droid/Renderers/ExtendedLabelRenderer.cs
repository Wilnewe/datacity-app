using System;
using Android.Content;
using Android.Views;
using Color = Xamarin.Forms.Color;
using Xamarin.Forms;
using DataCity.Droid.Renderers;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Text.Method;
using System.ComponentModel;
using Android.Graphics.Drawables.Shapes;
using Android.Text;
using Android.Content.Res;
using Android.OS;
using DataCity.Controls;

[assembly: ExportRenderer(typeof(ExtendedLabel), typeof(ExtendedLabelRenderer))]
namespace DataCity.Droid.Renderers
{
    public class ExtendedLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            var view = (ExtendedLabel)Element;

            if (Control != null)
            {
                Control.PaintFlags = view.Underline ? Control.PaintFlags | PaintFlags.UnderlineText : Control.PaintFlags &= ~PaintFlags.UnderlineText;
            }

        }
    }
}