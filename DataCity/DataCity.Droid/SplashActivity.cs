using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace DataCity.Droid
{
    [Activity(Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {

        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            base.OnCreate(savedInstanceState, persistentState);
            this.Window.AddFlags(WindowManagerFlags.Fullscreen);
        }

        protected override void OnResume()
        {
            base.OnResume();

            //Task startupWork = new Task(() => {
            //    //Task.Delay(1000);  // Simulate a bit of startup work.
            //});

            //startupWork.ContinueWith(t => {
            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
            //}, TaskScheduler.FromCurrentSynchronizationContext());

            //startupWork.Start();
        }
    }
}