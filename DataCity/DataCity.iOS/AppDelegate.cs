﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

using Foundation;
using UIKit;
using WindowsAzure.Messaging;
using DataCity.Common;

namespace DataCity.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        private SBNotificationHub Hub { get; set; }

        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());

            //UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.LightContent;
            /*
            if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,new NSSet());

                Console.WriteLine("UIDevice.CurrentDevice.CheckSystemVersion(8, 0)");

                app.RegisterUserNotificationSettings(pushSettings);
                app.RegisterForRemoteNotifications();
            }
            else
            {
                Console.WriteLine("UIDevice.CurrentDevice.CheckSystemVersion(10, 0)");
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                app.RegisterForRemoteNotificationTypes(notificationTypes);
            }*/


            // Process any potential notification data from launch
            ProcessNotification(options);

            // Register for Notifications
            var notificationTypes = UIUserNotificationType.Badge
                                    | UIUserNotificationType.Sound
                                    | UIUserNotificationType.Alert;

            var notificationSettings = UIUserNotificationSettings.GetSettingsForTypes(notificationTypes, null);

            UIApplication.SharedApplication.RegisterUserNotificationSettings(notificationSettings);

            return base.FinishedLaunching(app, options);
        }

        public override void DidRegisterUserNotificationSettings(UIApplication application, UIUserNotificationSettings notificationSettings)
        {
            UIApplication.SharedApplication.RegisterForRemoteNotifications();
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            // Process a notification received while the app was already open
            ProcessNotification(userInfo);
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            Console.WriteLine("RegisteredForRemoteNotifications");

            // Connection string from your azure dashboard
            //var cs = SBConnectionString.CreateListenAccess( new NSUrl("sb://aspiro.servicebus.windows.net"), "1xUUF4Z42ugV+6fxtfgpU1reZR6Cfzos/r3AThh94B4=");

            // Register our info with Azure
            //var hub = new SBNotificationHub(cs, Common.Configuration.NotificationHubName);


            var hub = new WindowsAzure.Messaging.SBNotificationHub("Endpoint=sb://aspiro.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=f6U2KVPCr3HSQ3p5DKINND606BvJ3Dqz2MjeBPMza5A=", "datacity");

            hub.RegisterNativeAsync(deviceToken, null, err => {

                if (err != null)
                {
                    Console.WriteLine("Error: " + err.Description);
                    //homeViewController.RegisteredForNotifications("Error: " + err.Description);
                }
                else
                {
                    Console.WriteLine("Success");
                    //homeViewController.RegisteredForNotifications("Successfully registered for notifications");
                }
            });
        }

        void ProcessNotification(NSDictionary userInfo)
        {
            if (userInfo == null)
                return;

            var apsKey = new NSString("aps");

            if (userInfo.ContainsKey(apsKey))
            {

                var alertKey = new NSString("alert");

                var aps = (NSDictionary)userInfo.ObjectForKey(apsKey);

                if (aps.ContainsKey(alertKey))
                {
                    var alert = (NSString)aps.ObjectForKey(alertKey);

                    //homeViewController.ProcessNotification(alert);
                    UIAlertView avAlert = new UIAlertView("Správa z DataCity", alert, null, "OK", null);
                    avAlert.Show();

                    //Console.WriteLine("Notification: " + alert);
                }
            }
        }



        /*
        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            Console.WriteLine("RegisteredForRemoteNotifications");

            Hub = new SBNotificationHub(Configuration.ListenConnectionString, Configuration.NotificationHubName);

            Hub.UnregisterAllAsync(deviceToken, (error) => {
                if (error != null)
                {
                    Console.WriteLine("Error calling Unregister: {0}", error.ToString());
                    return;
                }

                NSSet tags = null; // create tags if you want
                Hub.RegisterNativeAsync(deviceToken, tags, (errorCallback) => {
                    if (errorCallback != null)
                        Console.WriteLine("RegisterNativeAsync error: " + errorCallback.ToString());
                });
            });
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            Console.WriteLine("ReceivedRemoteNotification");
            ProcessNotification(userInfo, false);
        }

        void ProcessNotification(NSDictionary options, bool fromFinishedLaunching)
        {
            Console.WriteLine("ProcessNotification");

            // Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
            if (null != options && options.ContainsKey(new NSString("aps")))
            {
                //Get the aps dictionary
                NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;

                string alert = string.Empty;

                //Extract the alert text
                // NOTE: If you're using the simple alert by just specifying
                // "  aps:{alert:"alert msg here"}  ", this will work fine.
                // But if you're using a complex alert with Localization keys, etc.,
                // your "alert" object from the aps dictionary will be another NSDictionary.
                // Basically the JSON gets dumped right into a NSDictionary,
                // so keep that in mind.
                if (aps.ContainsKey(new NSString("alert")))
                    alert = (aps[new NSString("alert")] as NSString).ToString();

                //If this came from the ReceivedRemoteNotification while the app was running,
                // we of course need to manually process things like the sound, badge, and alert.
                if (!fromFinishedLaunching)
                {
                    //Manually show an alert
                    if (!string.IsNullOrEmpty(alert))
                    {
                        UIAlertView avAlert = new UIAlertView("Notification", alert, null, "OK", null);
                        avAlert.Show();
                    }
                }
            }
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            Console.WriteLine("DidReceiveRemoteNotification");

            NSDictionary aps = userInfo.ObjectForKey(new NSString("aps")) as NSDictionary;

            string alert = string.Empty;
            if (aps.ContainsKey(new NSString("alert")))
                alert = (aps[new NSString("alert")] as NSString).ToString();

            // Show alert
            if (!string.IsNullOrEmpty(alert))
            {
                UIAlertView avAlert = new UIAlertView("Notification", alert, null, "OK", null);
                avAlert.Show();
            }
        }


        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            new UIAlertView("Error registering push notifications", error.LocalizedDescription, null, "OK", null).Show();
        }
        */
    }
}
