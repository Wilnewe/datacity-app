using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms.Platform.iOS;
using DataCity.Controls;
using DataCity.iOS.Renderers;
using Xamarin.Forms;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer(typeof(ExtendedDatePicker), typeof(ExtendedDatePickerRenderer))]
namespace DataCity.iOS.Renderers
{

    public class ExtendedDatePickerRenderer : DatePickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                //            UIImageView envelopeView = new CGRect(20, 0, 30, 30);
                //            envelopeView.image = [UIImage imageNamed: @"comment-128.png"];
                //            envelopeView.contentMode = UIViewContentModeScaleAspectFit;
                //            UIView* test =  [[UIView alloc]initWithFrame: CGRectMake(20, 0, 30, 30)];
                //[test addSubview:envelopeView];
                //[self.textField.leftView setFrame:envelopeView.frame];
                //self.textField.leftView =test;
                //    self.textField.leftViewMode = UITextFieldViewModeAlways;
                var view = (ExtendedDatePicker)Element;

                if (view == null)
                    return;

                this.Control.Text = view.Placeholder;
                this.Control.TextColor = UIColor.White;

                UIImageView envelopeView = new UIImageView(new CGRect(10, 0, 30, 15));

                if (e != null && e.NewElement != null)
                {
                    
                    envelopeView.Image = new UIImage("calendar.png");
                }
                

                
                envelopeView.ContentMode = UIViewContentMode.ScaleAspectFit;
                //envelopeView.BackgroundColor = UIColor.Blue;


                UIView test = new UIView(new CGRect(0,0,0,30));
                //test.BackgroundColor = UIColor.Red;
                test.AddSubview(envelopeView);

                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.LeftView = test;
                Control.LeftView.Frame = envelopeView.Frame;



                // do whatever you want to the UITextField here!
                //Control.BackgroundColor = UIColor.FromRGB(204, 153, 255);


                //Control.BorderStyle = UITextBorderStyle.RoundedRect;

                Control.Layer.BorderColor = UIColor.White.CGColor;
                Control.Layer.BorderWidth = 1;
                Control.Layer.CornerRadius = 15;

                //Control.LeftViewMode = UITextFieldViewMode.Always;
                //Control.LeftView = new UIView(new CGRect(0, 0, 15, 0));
                //Control.LeftView = GetImageView("locked.png");
            }
        }

        private UIView GetImageView(string image)
        {
            return new UIImageView(GetImage(image));
        }

        private UIImage GetImage(string imagePath)
        {
            var uiImage = new UIImage(imagePath);
            return uiImage.Scale(new CGSize(this.Control.Font.LineHeight, this.Control.Font.LineHeight));
        }
    }

}