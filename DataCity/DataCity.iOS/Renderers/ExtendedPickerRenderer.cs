using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms.Platform.iOS;
using DataCity.Controls;
using DataCity.iOS.Renderers;
using Xamarin.Forms;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer(typeof(ExtendedPicker), typeof(ExtendedPickerRenderer))]
namespace DataCity.iOS.Renderers
{
    public class ExtendedPickerRenderer : PickerRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                //            UIImageView envelopeView = new CGRect(20, 0, 30, 30);
                //            envelopeView.image = [UIImage imageNamed: @"comment-128.png"];
                //            envelopeView.contentMode = UIViewContentModeScaleAspectFit;
                //            UIView* test =  [[UIView alloc]initWithFrame: CGRectMake(20, 0, 30, 30)];
                //[test addSubview:envelopeView];
                //[self.textField.leftView setFrame:envelopeView.frame];
                //self.textField.leftView =test;
                //    self.textField.leftViewMode = UITextFieldViewModeAlways;

                UIImageView envelopeView = new UIImageView(new CGRect(10, 0, 30, 15));

                

                if (e != null && e.NewElement != null)
                {

                    if (e.NewElement.StyleId == "Pohlavie")
                    {
                        envelopeView.Image = new UIImage("gender.png");
                    }
                    else
                    {
                        envelopeView.Image = new UIImage("China.png");
                    }

                    
                    e.NewElement.TextColor = Xamarin.Forms.Color.White;
                }

                UIPickerView picker = new UIPickerView();
 
                

                envelopeView.ContentMode = UIViewContentMode.ScaleAspectFit;
                //envelopeView.BackgroundColor = UIColor.Blue;
                Control.TextColor = UIColor.White;
                Control.TintColor = UIColor.White;

                //Control.Text = Control.Placeholder;

                UIView test = new UIView(new CGRect(0,0,0,30));
                //test.BackgroundColor = UIColor.Red;
                test.AddSubview(envelopeView);

                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.LeftView = test;
                Control.LeftView.Frame = envelopeView.Frame;


                //Toto nastavi title color
                Control.AttributedPlaceholder = new Foundation.NSAttributedString(Control.AttributedPlaceholder.Value, foregroundColor: UIColor.White);

                // do whatever you want to the UITextField here!
                //Control.BackgroundColor = UIColor.FromRGB(204, 153, 255);


                //Control.BorderStyle = UITextBorderStyle.RoundedRect;

                Control.Layer.BorderColor = UIColor.White.CGColor;
                Control.Layer.BorderWidth = 1;
                Control.Layer.CornerRadius = 15;

                //Control.LeftViewMode = UITextFieldViewMode.Always;
                //Control.LeftView = new UIView(new CGRect(0, 0, 15, 0));
                //Control.LeftView = GetImageView("locked.png");
            }
        }

        private UIView GetImageView(string image)
        {
            return new UIImageView(GetImage(image));
        }

        private UIImage GetImage(string imagePath)
        {
            var uiImage = new UIImage(imagePath);
            return uiImage.Scale(new CGSize(this.Control.Font.LineHeight, this.Control.Font.LineHeight));
        }
    }

}