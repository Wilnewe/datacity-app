﻿using System;
using System.Collections.Generic;
using System.Text;
using DataCity.Controls;
using DataCity.IOS.Renderers;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ExtendedLabel), typeof(ExtendedLabelRenderer))]
namespace DataCity.IOS.Renderers
{
    public class ExtendedLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            var view = (ExtendedLabel)Element;

            if (Control != null)
            {

                //Control.PaintFlags = view.Underline ? Control.PaintFlags | PaintFlags.UnderlineText : Control.PaintFlags &= ~PaintFlags.UnderlineText;



                var attrString = new NSMutableAttributedString(Control.Text);

                if (view.Underline)
                {
                    attrString.AddAttribute(UIStringAttributeKey.UnderlineStyle,
                        NSNumber.FromInt32((int)NSUnderlineStyle.Single),
                        new NSRange(0, attrString.Length));
                }

                //if (view.IsStrikeThrough)
                //{
                //    attrString.AddAttribute(UIStringAttributeKey.StrikethroughStyle,
                //        NSNumber.FromInt32((int)NSUnderlineStyle.Single),
                //        new NSRange(0, attrString.Length));
                //}

                Control.AttributedText = attrString;
            }

        }
    }
}