using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms.Platform.iOS;
using DataCity.Controls;
using DataCity.iOS.Renderers;
using Xamarin.Forms;
using UIKit;
using CoreGraphics;

[assembly: ExportRenderer(typeof(ExtendedEntry), typeof(MyEntryRenderer))]
namespace DataCity.iOS.Renderers
{

    public class MyEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                //            UIImageView envelopeView = new CGRect(20, 0, 30, 30);
                //            envelopeView.image = [UIImage imageNamed: @"comment-128.png"];
                //            envelopeView.contentMode = UIViewContentModeScaleAspectFit;
                //            UIView* test =  [[UIView alloc]initWithFrame: CGRectMake(20, 0, 30, 30)];
                //[test addSubview:envelopeView];
                //[self.textField.leftView setFrame:envelopeView.frame];
                //self.textField.leftView =test;
                //    self.textField.leftViewMode = UITextFieldViewModeAlways;

                UIImageView envelopeView = new UIImageView(new CGRect(10, 0, 30, 15));

                if (e != null && e.NewElement != null)
                {
                    if (e.NewElement.IsPassword)
                    {
                        envelopeView.Image = new UIImage("locked.png");
                    }
                    else
                    {
                        if (e.NewElement.Keyboard == Keyboard.Email)
                        {
                            envelopeView.Image = new UIImage("email.png");
                        }
                        else if (e.NewElement.Keyboard == Keyboard.Telephone)
                        {
                            envelopeView.Image = new UIImage("phone.png");
                        }
                        else
                        {
                            envelopeView.Image = new UIImage("user2.png");
                        }

                        
                    }
                }
                

                
                envelopeView.ContentMode = UIViewContentMode.ScaleAspectFit;


                UIView test = new UIView(new CGRect(0,0,0,30));

                test.AddSubview(envelopeView);

                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.LeftView = test;
                Control.LeftView.Frame = envelopeView.Frame;


                // do whatever you want to the UITextField here!
                //Control.BackgroundColor = UIColor.FromRGB(204, 153, 255);

                if (!Control.Enabled)
                {
                    Control.TextColor = UIColor.Gray;
                }
                


                //Control.BorderStyle = UITextBorderStyle.RoundedRect;

                Control.Layer.BorderColor = UIColor.White.CGColor;
                Control.Layer.BorderWidth = 1;
                Control.Layer.CornerRadius = 15;
                
                //Control.LeftViewMode = UITextFieldViewMode.Always;
                //Control.LeftView = new UIView(new CGRect(0, 0, 15, 0));
                //Control.LeftView = GetImageView("locked.png");
            }
        }

        private UIView GetImageView(string image)
        {
            return new UIImageView(GetImage(image));
        }

        private UIImage GetImage(string imagePath)
        {
            var uiImage = new UIImage(imagePath);
            return uiImage.Scale(new CGSize(this.Control.Font.LineHeight, this.Control.Font.LineHeight));
        }
    }

}