using System;
using System.ComponentModel;
using Xamarin.Forms;

using Xamarin.Forms.Platform.iOS;
using DataCity.Controls;
using DataCity.iOS.Renderers;
using CoreGraphics;

[assembly: ExportRenderer(typeof(CustomRadioButton), typeof(RadioButtonRenderer))]
namespace DataCity.iOS.Renderers
{
    public class RadioButtonRenderer : ViewRenderer<CustomRadioButton, RadioButtonView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<CustomRadioButton> e)
        {
            base.OnElementChanged(e);

            //BackgroundColor = Element.BackgroundColor.ToUIColor();

            if (Control == null)
            {
                var checkBox = new RadioButtonView((System.Drawing.RectangleF)Bounds);
                checkBox.TouchUpInside += (s, args) => Element.Checked = Control.Checked;

                SetNativeControl(checkBox);
            }




            Control.LineBreakMode = UIKit.UILineBreakMode.CharacterWrap;
            Control.VerticalAlignment = UIKit.UIControlContentVerticalAlignment.Top;
            if (e.NewElement != null)
            {
                Control.Text = e.NewElement.Text;
                Control.Checked = e.NewElement.Checked;
                Control.SetTitleColor(e.NewElement.TextColor.ToUIColor(), UIKit.UIControlState.Normal);
                Control.SetTitleColor(e.NewElement.TextColor.ToUIColor(), UIKit.UIControlState.Selected);
            }

        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            ResizeText();
        }

        private void ResizeText()
        {
            var text = this.Element.Text;


            var bounds = this.Control.Bounds;

            var width = this.Control.TitleLabel.Bounds.Width;

            var height = text.StringHeight(this.Control.Font, (float)width);

            var minHeight = string.Empty.StringHeight(this.Control.Font, (float)width);

            var requiredLines = Math.Round(height / minHeight, MidpointRounding.AwayFromZero);

            var supportedLines = Math.Round(bounds.Height / minHeight, MidpointRounding.ToEven);

            if (supportedLines != requiredLines)
            {
                bounds.Height += (float)(minHeight * (requiredLines - supportedLines));
                this.Control.Bounds = bounds;
                this.Element.HeightRequest = bounds.Height;
            }
        }

        //public override void Draw(System.Drawing.RectangleF rect)
        //{
        //    base.Draw(rect);
        //    this.ResizeText();
        //}



        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            switch (e.PropertyName)
            {
                case "Checked":
                    Control.Checked = Element.Checked;
                    break;
                case "Text":
                    Control.Text = Element.Text;
                    break;
                case "TextColor":
                    Control.SetTitleColor(Element.TextColor.ToUIColor(), UIKit.UIControlState.Normal);
                    Control.SetTitleColor(Element.TextColor.ToUIColor(), UIKit.UIControlState.Selected);
                    break;
                case "Element":
                    break;
                default:
                    System.Diagnostics.Debug.WriteLine("Property change for {0} has not been implemented.", e.PropertyName);
                    return;
            }
        }
    }
}