﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.Controls
{
    public class ExtendedLabel : Label
    {

        #pragma warning disable CS0618 // Type or member is obsolete
        public static readonly BindableProperty UnderlineProperty = BindableProperty.Create<ExtendedLabel, bool>(p => p.Underline, false);
        #pragma warning restore CS0618 // Type or member is obsolete

        public bool Underline
        {
            get
            {
                return this.GetValue<bool>(UnderlineProperty);
            }

            set
            {
                this.SetValue(UnderlineProperty, value);
            }
        }
    }
}
