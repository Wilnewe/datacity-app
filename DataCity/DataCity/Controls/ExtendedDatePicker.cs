﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.Controls
{
    public class ExtendedDatePicker : DatePicker
    {
        public static readonly BindableProperty TextProperty = BindableProperty.Create("Text", typeof(string), typeof(ExtendedDatePicker), string.Empty);

        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create("Placeholder", typeof(string), typeof(ExtendedDatePicker), string.Empty);

        public ExtendedDatePicker()
        {

            if (Device.OS == TargetPlatform.iOS)
            {
                TextColor = Color.White;
                BackgroundColor = Color.Transparent;
            }
            else
            {
                TextColor = Color.White;
            }
        }

        public string Text
        {
            get { return (string)this.GetValue(TextProperty); }
            set { this.SetValue(TextProperty, value); }
        }

        public string Placeholder
        {
            get { return (string)this.GetValue(PlaceholderProperty); }
            set { this.SetValue(PlaceholderProperty, value); }
        }
    }
}
