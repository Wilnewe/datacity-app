﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.Controls
{
    public class ExtendedButton : Button
    {
        public int PoolId;
        public string Slug;

        public ExtendedButton()
        {
            if (Device.OS == TargetPlatform.iOS)
            {
                BorderRadius = 23;
            }
            else
            {
                BorderRadius = 30;
            }
        }
    }
}
