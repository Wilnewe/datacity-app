﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.Controls
{
    public class ExtendedPicker : Picker
    {
        public ExtendedPicker()
        {

            if (Device.OS == TargetPlatform.iOS)
            {
                TextColor = Color.White;
                BackgroundColor = Color.Transparent;
            }
            else
            {
                TextColor = Color.White;
            }
        }
    }
}
