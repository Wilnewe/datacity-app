﻿using DataCity.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


using Xamarin.Forms;

namespace DataCity.Views.Kpi
{
    public partial class Primary : ContentPage
    {
        private readonly ViewModels.Kpi.PrimaryViewModel _viewModel;
        private ToolbarItem mapToolbarItem;

        public Primary()
        {
            InitializeComponent();

            var portal = JsonConvert.DeserializeObject<Portal>(DataCity.Helpers.Settings.Portal.ToString());
            var lang = DataCity.Helpers.Settings.ActualLocale;
            //web.Source = new Uri(String.Format("http://{0}.stage.datacity.global/{1}?layout=app", portal.Domain, lang));

            string token = JsonConvert.DeserializeObject<DataCity.Model.Account>(JObject.Parse(Helpers.Settings.Profil).SelectToken("data").ToString()).Token ;
            web.Source = new Uri(String.Format("http://{0}.stage.datacity.global/api/v1/dashboard?lang={1}&auth_token={2}", portal.Domain, lang, token));

            //          var htmlSource = new HtmlWebViewSource();
            //          htmlSource.Html = @"<html><body>
            //<h1>Xamarin.Forms</h1>
            //<p>Welcome to WebView.</p>
            //</body></html>";
            //          web.Source = htmlSource;

            /*
            try
            {
                var webRequest = System.Net.WebRequest.Create(String.Format("http://{0}.stage.datacity.global/api/v1/dashboard?lang={1}", portal.Domain, lang));
                if (webRequest != null)
                {
                    webRequest.Method = "GET";
                    webRequest.Headers["Authorization"] ="Basic dcmGV25hZFzc3VudDM6cGzdCdvQ=";
                    
                }
            }
            catch (Exception ex)
            {
                
            }*/

            BindingContext = _viewModel = new ViewModels.Kpi.PrimaryViewModel(this);
 


            //BindingContext = _viewModel = new ViewModels.Kpi.PrimaryViewModel(this);
            //NavigationPage.SetBackButtonTitle(this, "");

            ////var uri = new Uri(this.Content)
            //mapToolbarItem = new ToolbarItem("Map", "vectorLogoVertical.png", () =>
            //{
            //    Device.OpenUri(new Uri("https://datacity.global"));
            //});
            //mapToolbarItem.Priority = 1;
            //ToolbarItems.Add(mapToolbarItem);

            //var strurl = Helpers.Settings.UriKpi;
            //if (!string.IsNullOrEmpty(strurl))
            //{
            //    var uri = new Uri(strurl);
            //    web.Source = uri;
            //}
        }


        private void WebView_OnScriptNotify(object sender, EventArgs e)
        {
            //throw new Exception("dsadsa");
        }


        private void web_Navigated(object sender, WebNavigatedEventArgs e)
        {
            try
            {
                var strId = e.Url.Split('#')[1];
                int id = Int32.Parse(strId);
                var _selectedItem = _viewModel.Data.FirstOrDefault(o => o.Id == Int32.Parse(strId));

                _viewModel.Navigate(_selectedItem);

            }
            catch (Exception ex)
            {
                var i = 0;
            }
        }

        private void web_Navigating(object sender, WebNavigatingEventArgs e)
        {
            try
            {
                var strId = e.Url.Split('#')[1];
                int id = Int32.Parse(strId);
                var _selectedItem = _viewModel.Data.FirstOrDefault(o => o.Id == Int32.Parse(strId));

                _viewModel.Navigate(_selectedItem);

            }
            catch (Exception ex)
            {
                var i = 0;
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_viewModel.Data.Count > 0 || _viewModel.IsBusy)
                return;

            _viewModel.GetDataCommand.Execute(null);

        }
    }
}
