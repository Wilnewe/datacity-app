﻿using DataCity.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Account
{
    public  partial  class Login : ContentPage
    {
        private readonly ViewModels.Account.LoginViewModel _viewModel;
        //private ToolbarItem mapToolbarItem;

        public Login()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Account.LoginViewModel(this);

            NavigationPage.SetBackButtonTitle(this, "");
            NavigationPage.SetHasBackButton(this, false);
            NavigationPage.SetHasNavigationBar(this, false);

            //loader.BackgroundColor = new Color(0, 0, 0, 0.5);


            //BackgroundImage = "background.jpg";

            BtnLogin.Clicked += (sender, e) => { _viewModel.GetSubmitCommand.Execute(null); };
            /*
            mapToolbarItem = new ToolbarItem("Map", "map.png", () =>
            {
              '  //OnMapClick();
            });
            ToolbarItems.Add(mapToolbarItem);*/

            BtnRegister.Clicked += (sender, e) => {

                App.Current.MainPage.Navigation.PushAsync(new Account.Register(), false);
                //TaskUtils.SetDetailPage(new Account.Register());

            };


            BtnReset.Clicked += (sender, e) => {

                App.Current.MainPage.Navigation.PushAsync(new Account.LostPassword(), false);
                //TaskUtils.SetDetailPage(new Account.Register());

            };

            BtnBackToZoznam.Clicked += (sender, e) => {
                App.Current.MainPage.Navigation.PopAsync(false);
            };

            //BtnRegister.Clicked += async delegate
            //{
            //    var a = await PclStorage.ReadAccountFromFile();
            //};
        }
    }
}
