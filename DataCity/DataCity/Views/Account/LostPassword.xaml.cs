﻿using DataCity.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Account
{
    public  partial  class LostPassword : ContentPage
    {
        private readonly ViewModels.Account.LostPasswordViewModel _viewModel;
        //private ToolbarItem mapToolbarItem;

        public LostPassword()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Account.LostPasswordViewModel(this);

            NavigationPage.SetBackButtonTitle(this, "");
            NavigationPage.SetHasBackButton(this, false);
            NavigationPage.SetHasNavigationBar(this, false);

            //loader.BackgroundColor = new Color(0, 0, 0, 0.5);


            //BackgroundImage = "background.jpg";

            /*
            mapToolbarItem = new ToolbarItem("Map", "map.png", () =>
            {
              '  //OnMapClick();
            });
            ToolbarItems.Add(mapToolbarItem);*/

            BtnReset.Clicked += (sender, e) => {

                _viewModel.GetResetCommand.Execute(null);
            };

            BtnBackToZoznam.Clicked += (sender, e) => {
                App.Current.MainPage.Navigation.PopAsync(false);
            };

            //BtnRegister.Clicked += async delegate
            //{
            //    var a = await PclStorage.ReadAccountFromFile();
            //};
        }
    }
}
