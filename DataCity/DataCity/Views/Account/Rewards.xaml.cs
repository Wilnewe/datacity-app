﻿using DataCity.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Account
{
    public partial class Rewards : ContentPage
    {
        private readonly ViewModels.Account.RewardsViewModel _viewModel;

        public Rewards()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Account.RewardsViewModel(this);

            BtnBackToProfil.Clicked += (sender, e) => {
                TaskUtils.SetDetailPage(new Account.Profil());
            };

            lstRewards.HeightRequest = 0;
            _viewModel.Data.CollectionChanged += (sender, args) =>
            {
                lstRewards.HeightRequest = (_viewModel.Data.Count * 50) + 50;
            };
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_viewModel.Data.Count > 0 || _viewModel.IsBusy)
                return;
            _viewModel.GetRewardCommand.Execute(null);
        }
    }
}
