﻿using DataCity.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Account
{
    public partial class Profil : ContentPage
    {
        private readonly ViewModels.Account.ProfilViewModel _viewModel;
        private bool _selectedDate = false;
        public Profil()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Account.ProfilViewModel(this);

            DatumNarodenia.DateSelected += (s, e) => {
                _selectedDate = true;
            };

            foreach (var item in _viewModel.Portal.Languages)
            {
                Jazyk.Items.Add(item.Code);
            }

            Pohlavie.Items.Add(_viewModel.Resources["Muz"]);
            Pohlavie.Items.Add(_viewModel.Resources["Zena"]);

            Jazyk.SelectedIndex = Jazyk.Items.IndexOf(DataCity.Helpers.Settings.ActualLocale);

            BtnUlozit.Clicked += (sender, e) => {

                DataCity.Helpers.Settings.ActualLocale = Jazyk.SelectedItem.ToString();

                _viewModel.Data.BirthDate = _selectedDate ? DatumNarodenia.Date.ToString("yyyy-MM-dd") : null;

                switch (Pohlavie.SelectedIndex)
                {
                    case 0:
                        _viewModel.Data.Gender = "male";
                        break;
                    case 1:
                        _viewModel.Data.Gender = "female";
                        break;
                    default:
                        _viewModel.Data.Gender = "";
                        break;
                }

                _viewModel.PostProfilCommand.Execute(null);

                
                Application.Current.MainPage = new DataCity.Views.MainPage();
                TaskUtils.SetDetailPage(new Views.Account.Profil());
            };

            BtnVymazat.Clicked += async (sender, e) =>
            {

                var answer = await DisplayAlert("", _viewModel.Resources["ChceteVymazatProfil"].ToString(), _viewModel.Resources["Ano"].ToString(), _viewModel.Resources["Nie"].ToString());
                if (answer)
                {
                    _viewModel.DeleteProfilCommand.Execute(null);
                }


            };

            BtnRewards.Clicked += (sender, e) =>
            {

                //App.Current.MainPage.Navigation.PushAsync(new Account.Rewards(), false);
                TaskUtils.SetDetailPage(new Account.Rewards());
            };


            _viewModel.Data.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == "Gender")
                {
                    Pohlavie.SelectedIndex = ((Model.Register) sender).Gender == "male" ? 0 : 1;
                }

                if (args.PropertyName == "BirthDate")
                {
                    DatumNarodenia.Date = DateTime.Parse(((Model.Register) sender).BirthDate);
                }
            };

        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_viewModel.IsBusy)
                return;

            _viewModel.GetProfilCommand.Execute(null);

        }
    }
}
