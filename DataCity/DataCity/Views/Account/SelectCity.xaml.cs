﻿using DataCity.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Account
{
    public partial class SelectCity : ContentPage
    {
        private readonly ViewModels.Account.SelectCityViewModel _viewModel;
        public SelectCity()
        {
            InitializeComponent();

            BackgroundImage = "background.jpg";

            NavigationPage.SetBackButtonTitle(this, "");
            NavigationPage.SetHasBackButton(this, false);
            NavigationPage.SetHasNavigationBar(this, false);

            BindingContext = _viewModel = new ViewModels.Account.SelectCityViewModel(this);

            //BtnHlohovec.Clicked += (sender, e) => {

            //    Application.Current.MainPage = new DataCity.Views.MainPage();
            //    TaskUtils.SetDetailPage(new Views.Kpi.Primary());
            //};

            //BtnCity.GestureRecognizers.Add((new TapGestureRecognizer
            //{
            //    Command = new Command((o) =>
            //    {
            //        App.Current.MainPage.Navigation.PushAsync(new Views.Account.Login(), false);
            //    }),
            //    CommandParameter = BtnHlohovec
            //}));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_viewModel.Data.Count > 0 || _viewModel.IsBusy)
                return;

            _viewModel.GetDataCommand.Execute(null);

        }

        private void OnTapGestureRecognizerTapped(object sender, EventArgs e)
        {
            if (((TappedEventArgs)e).Parameter.ToString() == "active")
                App.Current.MainPage.Navigation.PushAsync(new Views.Account.Login(), false);
        }
    }
}
