﻿using DataCity.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Account
{
    public partial class Register : ContentPage
    {
        private readonly ViewModels.Account.RegisterViewModel _viewModel;
        private bool _selectedDate = false;
        public Register()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Account.RegisterViewModel(this);


            NavigationPage.SetBackButtonTitle(this, "");
            NavigationPage.SetHasBackButton(this, false);
            NavigationPage.SetHasNavigationBar(this, false);

            DatumNarodenia.DateSelected += (s, e) => {
                _selectedDate = true;
            };


            Pohlavie.Items.Add(_viewModel.Resources["Muz"]);
            Pohlavie.Items.Add(_viewModel.Resources["Zena"]);


            //BackgroundImage = "background.jpg";
            SetupButton(false);
            BtnRegister.Clicked += (sender, e) => {

                _viewModel.Data.BirthDate = _selectedDate ? DatumNarodenia.Date.ToString("yyyy-MM-dd") : null;

                switch (Pohlavie.SelectedIndex)
                {
                    case 0:
                        _viewModel.Data.Gender = "male";
                        break;
                    case 1:
                        _viewModel.Data.Gender = "female";
                        break;
                    default:
                        _viewModel.Data.Gender = "";
                        break;
                }

                _viewModel.GetSubmitCommand.Execute(null);

            };

            BtnLogin.Clicked += (sender, e) => {
                App.Current.MainPage.Navigation.PopAsync(false);
            };

            lblPravidla.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => {
                    var uri = $"http://{_viewModel.Portal.Domain}.stage.datacity.global/{DataCity.Helpers.Settings.ActualLocale}/tos";
                    Device.OpenUri(new Uri(uri));
                }),
            });

        }

        private void Switch_Toggled(object sender, ToggledEventArgs e)
        {
            SetupButton((bool)((Switch)sender).IsToggled);
        }

        private void SetupButton(bool status)
        {
            if (!status)
            {
                BtnRegister.BackgroundColor = Color.Transparent;
                BtnRegister.TextColor = Color.FromHex("#787878");
                BtnRegister.BorderColor = Color.FromHex("#787878");
            }
            else
            {
                BtnRegister.BackgroundColor = Color.FromHex("#009FE8");
                BtnRegister.BorderColor = Color.FromHex("#009FE8");
                BtnRegister.TextColor = Color.FromHex("#FFFFFF");
            }
            BtnRegister.IsEnabled = status;
        }
    }
}
