﻿using DataCity.Model;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views
{
    public partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += OnItemSelected;
        }


        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;
            if (item == null) return;

            if (item.TargetType == null)
            {
                item.TargetType = typeof(Views.Kpi.Primary);
            }

            if(!CrossConnectivity.Current.IsConnected && item.NeedInternet)
            {
                this.DisplayAlert("", "Pre túto funkcionalitu je potrebné pripojenie na internet", "OK");
                return;
            }

            DataCity.Helpers.Settings.UriKpi = item.Source == null ? "" : item.Source.ToString();

           Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType))
            {
                BarBackgroundColor = Color.FromHex("#000000"),
                BarTextColor = Color.FromHex("#FFFFFF"),
                Title = item.Title
            };


            MasterPage.Title = "Hlohovec";
            Title = "Hlohovec";


            MasterPage.ListView.SelectedItem = null;
            IsPresented = false;
        }



    }
}
