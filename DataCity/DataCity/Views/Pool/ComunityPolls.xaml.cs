﻿using DataCity.Controls;
using DataCity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Pool
{
    public partial class ComunityPolls : ContentPage
    {

        private readonly ViewModels.Pool.ComunityPoolViewViewModel _viewModel;
        private List<ComunityHelp> selected = new List<ComunityHelp>();

        public ComunityPolls()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Pool.ComunityPoolViewViewModel(this);

            var mapToolbarItem = new ToolbarItem("Map", "vectorLogoVertical.png", () =>
            {
                Device.OpenUri(new Uri("https://datacity.global"));
            });
            mapToolbarItem.Priority = 1;
            ToolbarItems.Add(mapToolbarItem);

            //MyRadiouGroup.CheckedChanged += MyRadiouGroup_CheckedChanged;
            bool rendered = false;
            _viewModel.PropertyChanged += (sender, e) =>
            {
                if (rendered || _viewModel.Data.Count() == 0)
                    return;

                rendered = true;
                foreach (var c in _viewModel.Data)
                {

                    Image bx = new Image();
                    bx.HeightRequest = 1;
                    bx.Source = "lineCopy.png";
                    bx.HorizontalOptions = LayoutOptions.FillAndExpand;
                    bx.Aspect = Aspect.AspectFill;
                    bx.Margin = new Thickness(0, 10, 0, 10);
                    scMainStack.Children.Add(bx);


                    Label l = new Label();
                    l.Text = c.Name;
                    l.FontSize = 18;
                    l.TextColor = Color.FromHex("#747474");
                    l.FontAttributes = FontAttributes.Bold;
                    l.Margin = new Thickness(20, 10, 20, 10);
                    scMainStack.Children.Add(l);

                    BindableRadioGroup bg = new BindableRadioGroup();
                   
                    bg.ItemsSource = c.Options;


                    bg.Margin = new Thickness(15, 0, 15, 10);
                    scMainStack.Children.Add(bg);
                    //content.Children.Add(p);


                    foreach (var q in c.Options)
                    {
                        selected.Add(new ComunityHelp() { Id = c.Id, Question = q.Id, Group = bg });
                        if (q.Voted)
                        {
                            bg.SelectedIndex = q.Id;
                        }
                    }

                    bg.IsEnabled = !c.Filled;
                    if (Device.OS == TargetPlatform.Android)
                    {
                        foreach (var item in bg.rads)
                        {
                            item.IsEnabled = !c.Filled;
                        }
                    }



                    /*
                    foreach (var q in c.Options)
                    {

                        
                        //Label t = new Label();
                        //t.Text = q.Name;
                        //t.Margin = new Thickness(20, 10, 20, 10);
                        //t.FontSize = 14;
                        //scMainStack.Children.Add(t);

                        BindableRadioGroup bg = new BindableRadioGroup();
                        bg.IsEnabled = !c.Filled;
                        bg.ItemsSource = q;

                        //if (q.Options.FirstOrDefault(o => o.Filled == true) != null)
                        //{
                        //    bg.SelectedIndex = q.Options.FirstOrDefault(o => o.Filled == true).Id;
                        //}

                        foreach (var item in bg.rads)
                        {
                            item.IsEnabled = !c.Filled;
                        }

                        bg.Margin = new Thickness(15, 0, 15, 10);
                        scMainStack.Children.Add(bg);
                        //content.Children.Add(p);
                        selected.Add(new ComunityHelp() { Id = c.Id, Question = q.Id, Group = bg });
                    }*/

                    ExtendedButton b = new ExtendedButton();
                    b.PoolId = c.Id;
                    b.IsEnabled = !c.Filled;
                    if (c.Filled)
                    {
                        b.BackgroundColor = Color.FromHex("#EEEEEE");
                        b.TextColor = Color.FromHex("#787878");
                        b.Text = _viewModel.Resources["UzSteHlasovali"].ToString();
                    }
                    else
                    {
                        b.BackgroundColor = Color.FromHex("#009FE8");
                        b.TextColor = Color.FromHex("#FFFFFF");
                        b.Text = _viewModel.Resources["OdoslatDotaznik"].ToString();

                    }
                    
                    b.Margin = new Thickness(20, 10, 20, 10);
                    scMainStack.Children.Add(b);
                    b.Clicked += B_Clicked;


                }
            };
        }

        private async void B_Clicked(object sender, EventArgs e)
        {
            ExtendedButton btn = (ExtendedButton)sender;

            _viewModel.QuestionId = btn.PoolId;

            var data = selected.Where(o => o.Id == btn.PoolId);
            foreach (var item in data)
            {
                BindableRadioGroup bg = item.Group;
                foreach (CustomRadioButton x in bg.rads)
                {
                    if (x.Checked)
                    {
                        _viewModel.RespondId = x.Id;
                        break;

                    }
                }
            }

            _viewModel.PostDataCommand.Execute(null);

            btn.IsEnabled = false;

            btn.BackgroundColor = Color.FromHex("#EEEEEE");
            btn.TextColor = Color.FromHex("#787878");
            btn.Text = _viewModel.Resources["UzSteHlasovali"].ToString();
        }

        void MyRadiouGroup_CheckedChanged(object sender, int e)
        {
            var radio = sender as CustomRadioButton;
            if (radio == null || radio.Id == -1) return;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_viewModel.Data.Count > 0 || _viewModel.IsBusy)
                return;

            _viewModel.GetDataCommand.Execute(null);

        }
   }


}
