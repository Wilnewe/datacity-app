﻿using DataCity.Common;
using DataCity.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views
{
    public partial class MasterPage
    {
        public ListView ListView => listView;

        private readonly ViewModels.MasterPageViewModel _viewModel;

        public MasterPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.MasterPageViewModel(this);

            Model.Account account = JsonConvert.DeserializeObject<Model.Account>(JObject.Parse(DataCity.Helpers.Settings.Profil).SelectToken("data").ToString());
            LoginName.Text = account.Email;

            //CrossConnectivity.Current.ConnectivityChanged += async (sender, args) =>
            //{
            //    await this.DisplayAlert("Informácia", "Je t=uto funkcionalitu je potrebné pripojenie na internet", "Zrušiť");
            //};
            var Portal = JsonConvert.DeserializeObject<Portal>(DataCity.Helpers.Settings.Portal.ToString());

            var LanguageList = new List<string>();
            foreach (var item in Portal.Languages) {
                LanguageList.Add(item.Code);
            }

            //pckLanguage.ItemsSource = LanguageList;

            //pckLanguage.SelectedIndex = pckLanguage.ItemsSource.IndexOf(DataCity.Helpers.Settings.ActualLocale);
            //pckLanguage.SelectedIndexChanged += (sender, e) =>
            //{
            //    DataCity.Helpers.Settings.ActualLocale = ((Picker)sender).SelectedItem.ToString();

            //    Application.Current.MainPage = new DataCity.Views.MainPage();
            //    TaskUtils.SetDetailPage(new Views.Feedback.CityFeedback());
            //};
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_viewModel.Data.Count > 0 || _viewModel.IsBusy)
                return;

            _viewModel.GetDataCommand.Execute(null);

        }

        private void ExtendedButton_Clicked(object sender, EventArgs e)
        {
            DataCity.Helpers.Settings.Profil = String.Empty;
            Application.Current.MainPage = new NavigationPage(new DataCity.Views.Account.SelectCity());
            Application.Current.MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
        }


        private void Profil_Clicked(object sender, EventArgs e)
        {
            DataCity.Common.TaskUtils.SetDetailPage(new Account.Profil());
            //this.Navigation.PushAsync(new Account.Profil());
        }
    }
}
