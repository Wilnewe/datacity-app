﻿using DataCity.Common;
using DataCity.Controls;
using DataCity.Model;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Feedback
{
    public partial class DropdownRatingTimestamp : ContentPage
    {
        private readonly ViewModels.Feedback.DetailViewModel _viewModel;
        List<ClickableImage> _images = new List<ClickableImage>();
        ClickableImage _lastClicked = null;
        private ToolbarItem mapToolbarItem;
        List<Option> data = new List<Option>();
        string selectedStar = "1";

        public DropdownRatingTimestamp(Model.CityFeedback item)
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Feedback.DetailViewModel(item, this);

            mapToolbarItem = new ToolbarItem("Map", "vectorLogoVertical.png", () =>
            {
                Device.OpenUri(new Uri("https://datacity.global"));
            });
            mapToolbarItem.Priority = 1;
            ToolbarItems.Add(mapToolbarItem);

            var dropdown = item.Components.FirstOrDefault(o => o.Key == "dropdown");
            var rating = item.Components.FirstOrDefault(o => o.Key == "rating");

            if (dropdown == null || rating == null)
            {
                return;
            }

            LblDropdown.Text = dropdown.Name;
            LblRating.Text = rating.Name;


            foreach (var v in dropdown.Values)
            {
                PckData.Items.Add(v);
            }



            for (int i = 0; i < 5; i++)
            {
                var newImg = new ClickableImage()
                {
                    Source = "star.png",
                    WidthRequest = 50,
                    HeightRequest = 50,
                    OnClick = new Command(o =>
                    {
                        _lastClicked = (ClickableImage)o;
                        var najdena = false;
                        foreach (var a in _images)
                        {
                            var newSource = najdena ? "star.png" : "star2.png";
                            if (!a.Source.Equals(newSource))
                            {
                                a.Source = newSource;
                            }
                            if (a == _lastClicked)
                            {
                                najdena = true;
                                selectedStar = (_images.IndexOf(a) + 1).ToString();
                            }
                        }
                    })
                };
                if (i == 0)
                {
                    // na zaciatku je oznacena jedna hviezdicka
                    _lastClicked = newImg;
                    //_viewModel.Form.Hodnotenie = 1;
                    newImg.Source = "star2.png";
                }
                _images.Add(newImg);
                StarsContainer.Children.Add(newImg);
            }

            BtnSend.Clicked += async (object sender, EventArgs e) =>
            {
                var c = _viewModel.Data.Components.First(y => y.Key == "dropdown");
                var d = _viewModel.Data.Components.First(y => y.Key == "rating");
                var f = _viewModel.Data.Components.First(y => y.Key == "timestamp-start");

                if(PckData.SelectedIndex==-1)
                {
                    await DisplayAlert("", _viewModel.Resources["ProsimVyplnteVsetkyUdaje"].ToString(), _viewModel.Resources["Zrusit"].ToString());
                    return;
                }

                var answer = await DisplayAlert("", _viewModel.Resources["ChceteOdoslatUdaje"].ToString(), _viewModel.Resources["Ano"].ToString(), _viewModel.Resources["Nie"].ToString());
                if (answer)
                {

                    c.SelectedValue = PckData.Items[PckData.SelectedIndex];
                    d.SelectedValue = selectedStar;
                    f.SelectedValue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    _viewModel.GetDataCommand.Execute(null);
                }
                
            };
        }
    }
}
