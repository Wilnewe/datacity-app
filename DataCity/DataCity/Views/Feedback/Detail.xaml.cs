﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Feedback
{
    public partial class Detail : ContentPage
    {
        private readonly ViewModels.Feedback.DetailViewModel _viewModel;
        private ToolbarItem mapToolbarItem;
        public Detail(Model.CityFeedback item)
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Feedback.DetailViewModel(item, this);


            mapToolbarItem = new ToolbarItem("Map", "vectorLogoVertical.png", () =>
            {
                //OnMapClick();
            });
            mapToolbarItem.Priority = 1;
            ToolbarItems.Add(mapToolbarItem);
        }
    }
}
