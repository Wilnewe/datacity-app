﻿using DataCity.Common;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Feedback
{
    public partial class GpsTimestampTimestamp : ContentPage
    {
        private readonly ViewModels.Feedback.DetailViewModel _viewModel;

        private string gpsStart = "0;0";
        private DateTime timestampStart;
        private DateTime timestampEnd;

        private bool rec = true;
        public GpsTimestampTimestamp(Model.CityFeedback item)
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Feedback.DetailViewModel(item, this);

            var mapToolbarItem = new ToolbarItem("Map", "vectorLogoVertical.png", () =>
            {
                Device.OpenUri(new Uri("https://datacity.global"));
            });
            mapToolbarItem.Priority = 1;
            ToolbarItems.Add(mapToolbarItem);

            BtnStart.IsVisible = true;
            BtnStop.IsVisible = false;
            BtnSend.IsVisible = false;

            BtnStart.Clicked += async (object sender, EventArgs e) =>
            {
                BtnStart.IsEnabled = false;
                try
                {
                    var locator = CrossGeolocator.Current;
                    var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
                    gpsStart = string.Format("{0};{1}", position.Latitude, position.Longitude);
                }
                catch (Exception)
                {
                    BtnStart.IsEnabled = true;
                    await DisplayAlert("", _viewModel.Resources["NepodariloSaZistitVasuPolohu"].ToString(), _viewModel.Resources["Ok"].ToString());
                    return;
                }


                Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                {
                    if (rec)
                        lblTime.Text = (timestampStart - DateTime.Now).ToString(@"hh\:mm\:ss");
                    return rec;
                });

                rec = true;
                timestampStart = DateTime.Now;

                BtnStart.IsVisible = false;
                BtnStop.IsVisible = true;
                BtnSend.IsVisible = false;
            };

            BtnStop.Clicked += (object sender, EventArgs e) =>
            {
                rec = false;
                timestampEnd = DateTime.Now;

                BtnStart.IsVisible = false;
                BtnStop.IsVisible = false;
                BtnSend.IsVisible = true;
            };

            BtnSend.Clicked += async (object sender, EventArgs e) =>
            {
                var answer = await DisplayAlert("", _viewModel.Resources["ChceteOdoslatUdaje"].ToString(), _viewModel.Resources["Ano"].ToString(), _viewModel.Resources["Nie"].ToString());
                if (answer)
                {
                    _viewModel.Data.Components.First(y => y.Key == "timestamp-start").SelectedValue = timestampStart.ToString("yyyy-MM-dd HH:mm:ss");
                    _viewModel.Data.Components.First(y => y.Key == "timestamp-end").SelectedValue = timestampEnd.ToString("yyyy-MM-dd HH:mm:ss");
                    _viewModel.Data.Components.First(y => y.Key == "gps-start").SelectedValue = gpsStart;

                    _viewModel.GetDataCommand.Execute(null);
                }
            };

            BtnReset.Clicked += async (object sender, EventArgs e) =>
            {
                var answer = await DisplayAlert("", _viewModel.Resources["PrajeteSiResetovatMeranie"].ToString(), _viewModel.Resources["Ano"].ToString(), _viewModel.Resources["Nie"].ToString());
                if (answer)
                {
                    rec = false;
                    lblTime.Text = "00:00:00";

                    BtnStart.IsEnabled = true;
                    BtnStart.IsVisible = true;
                    BtnStop.IsVisible = false;
                    BtnSend.IsVisible = false;

                }
            };
        }
    }
}
