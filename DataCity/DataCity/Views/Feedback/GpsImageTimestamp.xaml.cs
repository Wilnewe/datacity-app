﻿using DataCity.Controls;
using DataCity.Interfaces;
using DataCity.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Media;

using Xamarin.Forms;
using DataCity.Common;
using Plugin.Media.Abstractions;

namespace DataCity.Views.Feedback
{
    public partial class GpsImageTimestamp : ContentPage
    {
        private readonly ViewModels.Feedback.DetailViewModel _viewModel;
        private ToolbarItem mapToolbarItem;
        List<ClickableImage> _images = new List<ClickableImage>();
        private string gpsStart = "0;0";

        public GpsImageTimestamp(Model.CityFeedback item)
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Feedback.DetailViewModel(item, this);

            mapToolbarItem = new ToolbarItem("Map", "vectorLogoVertical.png", () =>  {
                Device.OpenUri(new Uri("https://datacity.global"));
            });
            mapToolbarItem.Priority = 1;
            ToolbarItems.Add(mapToolbarItem);

            var image = item.Components.FirstOrDefault(o => o.Key == "image");
 
            if (image == null)
            {
                return;
            }

            LblImage.Text = image.Name;


            AddPhotoButton.OnClick = new Command(async o =>
            {
                MediaFile file = null;
                var action = await DisplayActionSheet(_viewModel.Resources["PridatFotografiu"].ToString(), _viewModel.Resources["Zrusit"].ToString(), null, _viewModel.Resources["ZGalerie"].ToString(), _viewModel.Resources["ZFotoapataru"].ToString());
                if (action == _viewModel.Resources["ZGalerie"].ToString())
                {
                    await Task.Delay(100);
                    file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                    {
                        PhotoSize = Plugin.Media.Abstractions.PhotoSize.Small
                    });
                }
                else
                {
                    await Task.Delay(100);
                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        await DisplayAlert("", _viewModel.Resources["KameraNiejeDostupna"].ToString(), _viewModel.Resources["Ok"].ToString());
                        return;
                    }

                    file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                    {
                        PhotoSize = Plugin.Media.Abstractions.PhotoSize.Small,
                        Directory = "Sample",
                        Name = "test.jpg"
                    });


                }

                if (file == null)
                    return;


                AddPhotoButton.Source = file.Path;
                _viewModel.SelectedImage = file.GetStream();
            });

            BtnSend.Clicked += async (object sender, EventArgs e) =>
            {
                BtnSend.IsEnabled = false;
                try
                {
                    var locator = CrossGeolocator.Current;
                    var position = await locator.GetPositionAsync(timeoutMilliseconds: 10000);
                    gpsStart = string.Format("{0};{1}", position.Latitude, position.Longitude);
                }
                catch (Exception)
                {
                    await DisplayAlert("", _viewModel.Resources["NepodariloSaZistitPolohu"].ToString(), _viewModel.Resources["Ok"].ToString());
                    BtnSend.IsEnabled = true;
                    return;
                }

                BtnSend.IsEnabled = true;
                _viewModel.IsBusy = false;

                var answer = await DisplayAlert("", _viewModel.Resources["ChceteOdoslatUdaje"].ToString(), _viewModel.Resources["Ano"].ToString(), _viewModel.Resources["Nie"].ToString());
                if (answer)
                {

                    _viewModel.Data.Components.First(y => y.Key == "gps-start").SelectedValue = gpsStart;
                    _viewModel.Data.Components.First(y => y.Key == "timestamp-start").SelectedValue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    _viewModel.GetDataCommand.Execute(null);
                }
            };
        }
    }
}
