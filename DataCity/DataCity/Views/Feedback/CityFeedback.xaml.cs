﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace DataCity.Views.Feedback
{
    public partial class CityFeedback : ContentPage
    {
        private readonly ViewModels.Feedback.CityFeedbackViewModel _viewModel;
        private ToolbarItem mapToolbarItem;
        public CityFeedback()
        {
            InitializeComponent();
            BindingContext = _viewModel = new ViewModels.Feedback.CityFeedbackViewModel(this);

            mapToolbarItem = new ToolbarItem("Map", "vectorLogoVertical.png", () =>
            {
                Device.OpenUri(new Uri("https://datacity.global"));
            });
            mapToolbarItem.Priority = 1;
            ToolbarItems.Add(mapToolbarItem);

            NavigationPage.SetBackButtonTitle(this, "");
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_viewModel.Data.Count > 0 || _viewModel.IsBusy)
                return;

            _viewModel.GetDataCommand.Execute(null);

        }
    }
}
