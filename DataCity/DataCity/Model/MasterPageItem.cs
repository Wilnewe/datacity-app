﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCity.Model
{
    public class MasterPageItem
    {

        [JsonProperty(PropertyName = "name")]
        public string Title { get; set; }

        public string IconSource { get; set; }

        public Type TargetType { get; set; }

        public int Type { get; set; }

        [JsonProperty(PropertyName = "url")]
        public string Source { get; set; }

        public bool NeedInternet { get; set; } = true;
    }
}
