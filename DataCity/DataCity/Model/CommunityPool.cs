﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCity.Model
{
    public class CommunityPool
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        public bool Filled
        {
            get
            {
                return this.Options.Where(o => o.Voted == true).Count() > 0;
            }
        }

        [JsonProperty(PropertyName = "votes")]
        public int Votes { get; set; }

        [JsonProperty(PropertyName = "options")]
        public List<Option> Options { get; set; }

    }

    public class Question
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "options")]
        public List<Option> Options { get; set; }
    }

    public class Option
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "is_voted")]
        public bool Voted { get; set; }

        [JsonProperty(PropertyName = "votes")]
        public int Votes { get; set; }
    }
}

//[{"id":1,"name":"P\u00e1\u010di sa V\u00e1m Hlohovec?","is_donut_chart":true,"votes":5,"publish_date":"2017-10-23","expire_date":"2017-11-14","options":[{"id":1,"name":"\u00e1no","rank":1,"is_voted":true,"votes":3},{"id":2,"name":"nie","rank":2,"votes":0},{"id":3,"name":"ja nie som odtialto","rank":3,"votes":2}]},{"id":3,"name":"fsdfsd","is_donut_chart":true,"votes":2,"publish_date":"2017-11-13","expire_date":null,"options":[{"id":8,"name":"sd","rank":1,"is_voted":true,"votes":2},{"id":9,"name":"sd","rank":2,"votes":0}]}]