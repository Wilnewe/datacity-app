﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCity.Model
{
    public class Message
    {
        public string Msg { get; set; }
    }

    public class Err
    {
        [JsonProperty(PropertyName = "error")]
        public string[] Kpi { get; set; }

        [JsonProperty(PropertyName = "emial")]
        public string[] Email { get; set; }
    }
}
