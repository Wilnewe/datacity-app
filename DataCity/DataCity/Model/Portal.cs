﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCity.Model
{
    public class Portal
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "domain")]
        public string Domain { get; set; }


        [JsonProperty(PropertyName = "image_path")]
        public string ImagePath { get; set; }

        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }

        [JsonProperty(PropertyName = "language")]
        public PortalLanguage Language { get; set; }


        [JsonProperty(PropertyName = "languages")]
        public List<PortalLanguage> Languages { get; set; }
    }

    public class PortalLanguage
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }

    }
}
