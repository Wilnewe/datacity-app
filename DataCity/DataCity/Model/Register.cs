﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvvmHelpers;

namespace DataCity.Model
{
    public class Register : ObservableObject
    {
        private string _firstName = string.Empty;
        [JsonProperty(PropertyName = "first_name")]
        public string FirstName
        {
            get
            {
                return this._firstName;
            }
            set
            {
                this.SetProperty<string>(ref this._firstName, value, nameof(FirstName), (Action)null);
            }
        }

        private string _lastName = string.Empty;
        [JsonProperty(PropertyName = "last_name")]
        public string LastName
        {
            get
            {
                return this._lastName;
            }
            set
            {
                this.SetProperty<string>(ref this._lastName, value, nameof(LastName), (Action)null);
            }
        }


        private string _gender = string.Empty;
        [JsonProperty(PropertyName = "gender")]
        public string Gender
        {
            get
            {
                return this._gender;
            }
            set
            {
                this.SetProperty<string>(ref this._gender, value, nameof(Gender), (Action)null);
            }
        }


        private string _phone = string.Empty;
        [JsonProperty(PropertyName = "phone")]
        public string Phone
        {
            get
            {
                return this._phone;
            }
            set
            {
                this.SetProperty<string>(ref this._phone, value, nameof(Phone), (Action)null);
            }
        }

        private string _birthDate = string.Empty;
        [JsonProperty(PropertyName = "birth_date")]
        public string BirthDate
        {
            get
            {
                return this._birthDate;
            }
            set
            {
                this.SetProperty<string>(ref this._birthDate, value, nameof(BirthDate), (Action)null);
            }
        }


        private string _email = string.Empty;
        [JsonProperty(PropertyName = "email")]
        public string Email
        {
            get
            {
                return this._email;
            }
            set
            {
                this.SetProperty<string>(ref this._email, value, nameof(Email), (Action)null);
            }
        }

        private string _password = string.Empty;
        [JsonProperty(PropertyName = "password")]
        public string Password
        {
            get
            {
                return this._password;
            }
            set
            {
                this.SetProperty<string>(ref this._password, value, nameof(Password), (Action)null);
            }
        }


        private string _passwordConfirmation = string.Empty;
        [JsonProperty(PropertyName = "password_confirmation")]
        public string Password_confirmation
        {
            get
            {
                return this._passwordConfirmation;
            }
            set
            {
                this.SetProperty<string>(ref this._passwordConfirmation, value, nameof(Password_confirmation), (Action)null);
            }
        }

        private string _rewardPointsEarned = string.Empty;
        [JsonProperty(PropertyName = "reward_points_earned")]
        public string RewardPointsEarned
        {
            get
            {
                return this._rewardPointsEarned;
            }
            set
            {
                this.SetProperty<string>(ref this._rewardPointsEarned, value, nameof(RewardPointsEarned), (Action)null);
            }
        }

        private string _rewardPointsReceived = string.Empty;
        [JsonProperty(PropertyName = "reward_points_received")]
        public string RewardPointsReceived
        {
            get
            {
                return this._rewardPointsReceived;
            }
            set
            {
                this.SetProperty<string>(ref this._rewardPointsReceived, value, nameof(RewardPointsReceived), (Action)null);
            }
        }

        private string _rewardPointsActual= string.Empty;
        [JsonProperty(PropertyName = "reward_points_actual")]
        public string RewardPointsActual
        {
            get
            {
                return this._rewardPointsActual;
            }
            set
            {
                this.SetProperty<string>(ref this._rewardPointsActual, value, nameof(RewardPointsActual), (Action)null);
            }
        }

        
    }
}
