﻿using DataCity.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCity.Model
{
    public class Language
    {

        [JsonProperty(PropertyName = "translations")]
        public Dictionary<string, Dictionary<string, MyDictionary<string, string>>> Translation { get; set; }
    }

}
