﻿using DataCity.Common;
using DataCity.Interfaces;
using DataCity.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using DataCity.Model;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;

[assembly: Dependency(typeof(RestDataStore))]
namespace DataCity.Services
{
    public class RestDataStore : IDataStore
    {
        private readonly HttpClient _client;

        public RestDataStore()
        {
            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes("datacity:odyzeo"));

            _client = new HttpClient();
            _client.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        }

        private static string DomainUri => string.Format(Configuration.RestParametrizedUri, JsonConvert.DeserializeObject<Portal>(JObject.Parse(Helpers.Settings.Portal).ToString()).Domain);

        private static string Token => JsonConvert.DeserializeObject<Account>(JObject.Parse(Helpers.Settings.Profil).SelectToken("data").ToString()).Token;

        private static string DefaultUri => Common.Configuration.RestUri;

        public async Task<IEnumerable<Portal>> GetPortals()
        {
            var uri = $"{DefaultUri}portals";
            var response = await _client.GetAsync(uri);
            if (!response.IsSuccessStatusCode)
                throw new InvalidOperationException(response.ToString() ?? "!IsSuccessStatusCode");

            var content = await response.Content.ReadAsStringAsync();
            return  JsonConvert.DeserializeObject<List<Portal>>(content);
        }

        public async void GetLanguages()
        {
            var uri = $"{DefaultUri}languages/mobile";
            using (var response = await _client.GetAsync(uri))
            {
                if (response.IsSuccessStatusCode)
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    Helpers.Settings.Language = responseString;
                }
                else
                {
                    throw new InvalidOperationException(response.ToString() ?? "!IsSuccessStatusCode");
                }
            }
        }

        public async Task<IEnumerable<MasterPageItem>> GetKpi()
        {
            var uri = $"{DomainUri}kpi-list/{Helpers.Settings.ActualLocale}";
            using (var response = await _client.GetAsync(uri))
            {
                if (!response.IsSuccessStatusCode)
                    throw new InvalidOperationException(response.ToString() ?? "!IsSuccessStatusCode");

                var content = await response.Content.ReadAsStringAsync();
                var data = JObject.Parse(content).SelectToken("menu_items").ToString();
                return JsonConvert.DeserializeObject<List<MasterPageItem>>(data);
            }
        }

        public async Task<IEnumerable<RewardPoint>> GetRewardPoints()
        {
            var uri = $"{DomainUri}profile/reward-points?lang={DataCity.Helpers.Settings.ActualLocale}";
            using (var response = await _client.GetAsync(uri))
            {
                if (!response.IsSuccessStatusCode)
                    throw new InvalidOperationException(response.ToString() ?? "!IsSuccessStatusCode");

                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<RewardPoint>>(content);
            }
        }

        public async Task<IEnumerable<CommunityPool>> GetComunityPool()
        {
            var uri = $"{DomainUri}poll?lang={Helpers.Settings.ActualLocale}";
            using (var response = await _client.GetAsync(uri))
            {
                if (!response.IsSuccessStatusCode)
                    throw new InvalidOperationException(response.ToString() ?? "!IsSuccessStatusCode");

                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<CommunityPool>>(content);
            }
            
        }

        public async Task<bool> PostLogin(Login data)
        {
            var result = new Account();
            var uri = $"{DomainUri}login?lang={Helpers.Settings.ActualLocale}";

            var content = new StringContent(JsonConvert.SerializeObject(data));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await _client.PostAsync(uri, content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                //iba test ci je vsetko v poriadku
                var account = JsonConvert.DeserializeObject<Account>(JObject.Parse(responseString).SelectToken("data").ToString());
                DataCity.Helpers.Settings.Profil = responseString;
            }
            else
            {
                var err = await response.Content.ReadAsStringAsync();
                TaskUtils.CreateOperationException(err);
            }

            return true;
        }

        public async Task<Message> PostRegistrer(Register data)
        {
            var result = new Message();
            var uri = $"{DomainUri}register?lang={DataCity.Helpers.Settings.ActualLocale}";

            var content = new StringContent(JsonConvert.SerializeObject(data));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await _client.PostAsync(uri, content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var a = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                result = JsonConvert.DeserializeObject<Message>(a);
            }
            else
            {
                var err = await response.Content.ReadAsStringAsync();
                DataCity.Common.TaskUtils.CreateOperationException(err);
            }
            return result;
        }

        public async Task<Message> PostProfil(Register data)
        {
            var result = new Message();
            var uri = $"{DomainUri}profile?lang={DataCity.Helpers.Settings.ActualLocale}";
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

            var content = new StringContent(JsonConvert.SerializeObject(data));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await _client.PostAsync(uri, content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var a = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                result = JsonConvert.DeserializeObject<Message>(a);
            }
            else
            {
                var err = await response.Content.ReadAsStringAsync();
                DataCity.Common.TaskUtils.CreateOperationException(err);
            }
            return result;
        }

        public async Task<Register> GetProfil()
        {
            var uri = $"{DomainUri}profile";
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            var response = await _client.GetAsync(uri);
            if (!response.IsSuccessStatusCode)
                throw new InvalidOperationException(response?.ToString() ?? "!IsSuccessStatusCode");

            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Register>(content);
        }

        public async Task<Message> DeleteProfil()
        {
            var result = new Message();
            var uri = $"{DomainUri}profile?lang={DataCity.Helpers.Settings.ActualLocale}";
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

            var response = await _client.DeleteAsync(uri).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var a = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                result = JsonConvert.DeserializeObject<Message>(a);
            }
            else
            {
                var err = await response.Content.ReadAsStringAsync();
                DataCity.Common.TaskUtils.CreateOperationException(err);
            }
            return result;
        }

        public async Task<Message> PostResetPassword(Account data)
        {
            var result = new Message();
            var uri = $"{DomainUri}password-reset";

            var content = new StringContent(JsonConvert.SerializeObject(data));
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await _client.PostAsync(uri, content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var a = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                result = JsonConvert.DeserializeObject<Message>(a);
            }
            else
            {
                var err = await response.Content.ReadAsStringAsync();
                DataCity.Common.TaskUtils.CreateOperationException(err);
            }
            return result;
        }

        public async Task<IEnumerable<CityFeedback>> GetCityFeedback()
        {
            var items = new List<CityFeedback>();
            var uri = $"{DomainUri}kpi/crowdsourced?lang={DataCity.Helpers.Settings.ActualLocale}";

            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            var response = await _client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {

                try
                {
                    var content = await response.Content.ReadAsStringAsync();
                    //var data = JObject.Parse(content).SelectToken("data").ToString();
                    items = JsonConvert.DeserializeObject<List<CityFeedback>>(content);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            else
            {
                throw new InvalidOperationException(response?.ToString() ?? "!IsSuccessStatusCode");
            }
            return items;
        }

        public async Task<bool> PostComunityPool(int questionId, int respondId)
        {
            var data = new Dictionary<string, int> {{"option", respondId}};

            var uri = $"{DomainUri}poll/{questionId}?lang={DataCity.Helpers.Settings.ActualLocale}";

            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
            var json = JsonConvert.SerializeObject(data);
            var content = new StringContent(json);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await _client.PostAsync(uri, content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var responseString = await response.Content.ReadAsStringAsync();
            }
            else
            {
                var err = await response.Content.ReadAsStringAsync();
                DataCity.Common.TaskUtils.CreateOperationException(err);
            }

            return true;
        }

        public async Task<bool> PostKpi(CityFeedback data, Stream image)
        {
            var uri = $"{DomainUri}{"kpi/"}{data.Id.ToString()}{"/crowdsourced?lang="}{DataCity.Helpers.Settings.ActualLocale}";

            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

            MultipartFormDataContent formContent = new MultipartFormDataContent();

            if (image != null)
            {
                //HttpContent content = new StringContent("image");
                //formContent.Add(content, "image");
                HttpContent content = new StreamContent(image);

                content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = "image",
                    FileName = "image.png"
                };
                formContent.Add(content);
            }


            formContent.Add(new StringContent(data.Id.ToString()), String.Format("\"{0}\"", "id"));

            foreach (var item in data.Components)
            {
                if (item.Key == "image")
                    continue;

                formContent.Add(new StringContent(item.SelectedValue), String.Format("\"{0}\"", item.Key));

            }




            var response = await _client.PostAsync(uri, formContent).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                //await PclStorage.SerializeAccount(responseString);
            }
            else
            {
                var err = await response.Content.ReadAsStringAsync();
                DataCity.Common.TaskUtils.CreateOperationException(err);
            }

            return true;
        }

        public async Task<bool> PostKpiImage(Stream image, int id)
        {
            var uri = $"{Common.Configuration.RestUri}kpi-attachments/{id}/image";

            var form = new MultipartFormDataContent();
            HttpContent content = new StringContent("fileToUpload");
            form.Add(content, "fileToUpload");
            content = new StreamContent(image);


            content.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
            {
                Name = "fileToUpload",
                FileName = "image.png"
            };
            form.Add(content);


            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

            var response = await _client.PostAsync(uri, form).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var responseString = await response.Content.ReadAsStringAsync();
                //await PclStorage.SerializeAccount(responseString);
            }
            else
            {
                throw new InvalidOperationException(response?.ToString() ?? "!IsSuccessStatusCode");
            }

            return true;
        }

      
    }
}
