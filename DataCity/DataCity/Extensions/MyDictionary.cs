﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCity.Extensions
{
    public class MyDictionary<TKey, TValue> : Dictionary<TKey, TValue>
    {
        //public new TValue this[TKey key]
        //{
        //    get
        //    {
        //        TValue value;
        //        if (!TryGetValue(key, out value))
        //        {
        //            value = Activator.CreateInstance<TValue>();
        //            Add(key, value);
        //        }
        //        return value;
        //    }
        //    set { base[key] = value; }
        //}


        public new string this[TKey key]
        {
            get
            {
                TValue value;
                if (!TryGetValue(key, out value))
                {
                    return "*" + key + "*";
                }
                else
                {
                    return value.ToString();
                }

            }
        }
    }
}
