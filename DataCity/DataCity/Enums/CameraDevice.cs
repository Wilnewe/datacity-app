﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCity.Enums
{
    public enum CameraDevice
    {
        Rear,
        Front
    }
}
