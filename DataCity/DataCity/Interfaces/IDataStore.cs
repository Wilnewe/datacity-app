﻿using DataCity.Model;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DataCity.Interfaces
{
    public interface IDataStore
    {
        Task<Register> GetProfil();
        Task<Message> PostProfil(Register data);
        Task<Message> DeleteProfil();
        Task<IEnumerable<RewardPoint>> GetRewardPoints();
        Task<Message> PostResetPassword(Account data);
        Task<Message> PostRegistrer(Register model);
        Task<bool> PostLogin(Login model);
        Task<IEnumerable<CommunityPool>> GetComunityPool();
        Task<IEnumerable<MasterPageItem>> GetKpi();
        Task<IEnumerable<Portal>> GetPortals();
        Task<IEnumerable<CityFeedback>> GetCityFeedback();
        Task<bool> PostComunityPool(int respondId, int id);
        Task<bool> PostKpi(CityFeedback data, Stream image);
        void GetLanguages();
    }
}
