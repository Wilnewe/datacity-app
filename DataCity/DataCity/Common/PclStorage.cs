﻿using DataCity.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
/*
namespace DataCity.Common
{
    public static class PclStorage
    {
        private const string AccountFolder = "Account";
        private const string AccountFileName = "account.json";

        public static async Task SerializeAccount(Account account)
        {
            var folder = await NavigateToFolder();
            IFile file = await folder.CreateFileAsync(AccountFileName, CreationCollisionOption.ReplaceExisting);
            var data = JsonConvert.SerializeObject(account);
            await file.WriteAllTextAsync(data);
        }

        public static async Task SerializeAccount(string account)
        {
            var folder = await NavigateToFolder();
            IFile file = await folder.CreateFileAsync(AccountFileName, CreationCollisionOption.ReplaceExisting);
            await file.WriteAllTextAsync(account);
        }

        public static async Task<Account> ReadAccountFromFile()
        {
            var folder = await NavigateToFolder();

            if ((await folder.CheckExistsAsync(AccountFileName)) == ExistenceCheckResult.NotFound)
            {
                return new Account();
            }
            IFile file = await folder.GetFileAsync(AccountFileName);
            var data = await file.ReadAllTextAsync();
            if (string.IsNullOrEmpty(data)) return new Account();

            return JsonConvert.DeserializeObject<Account>(JObject.Parse(data).SelectToken("data").ToString());
        }


        private static async Task<IFolder> NavigateToFolder()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder folder = await rootFolder.CreateFolderAsync(AccountFolder, CreationCollisionOption.OpenIfExists);
            return folder;
        }

    }
}*/
