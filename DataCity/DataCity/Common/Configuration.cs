﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCity.Common
{
    public static class Configuration
    {
        public const int HttpClientMaxResponseContentBufferSize = 20 * 1024 * 1024; // 20MB
        public const string RestUri = "http://stage.datacity.global/api/v1/";
        public const string RestParametrizedUri = "http://{0}.stage.datacity.global/api/v1/";

        public const string SenderID = "382185160970"; // Google API Project Number
        public const string ListenConnectionString = "Endpoint=sb://datacity.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=1xUUF4Z42ugV+6fxtfgpU1reZR6Cfzos/r3AThh94B4=";
        public const string NotificationHubName = "datacity";

        public const int GetPositionTimeoutMilliseconds = 100;
    }
}
