﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.Common
{
    public static class TaskUtils
    {
        public static Task<T> TaskFromResult<T>(T result)
        {
            var tcs = new TaskCompletionSource<T>();
            tcs.SetResult(result);
            return tcs.Task;
        }

        public static void SetDetailPage(ContentPage page)
        {
            var masterDetailPage = Application.Current.MainPage as MasterDetailPage;
            masterDetailPage.Detail = new NavigationPage(page)
            {
                BarBackgroundColor = Color.FromHex("#000000"),
                BarTextColor = Color.FromHex("#FFFFFF")
            };
            

        }

        public static void CreateOperationException(string err)
        {
            Newtonsoft.Json.Linq.JObject jsonConverter = (JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(err);
            var msgs = new StringBuilder();
            foreach (var x in jsonConverter)
            {
                if (((JToken)x.Value).Type == JTokenType.String)
                {
                    msgs.AppendLine(x.Value.ToString());
                }
                else
                {
                    msgs.AppendLine(string.Join("\n", x.Value));
                }
                
            }
            throw new OperationCanceledException(msgs.ToString());
        }
    }
}
