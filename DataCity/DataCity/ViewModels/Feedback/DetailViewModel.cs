﻿using DataCity.Extensions;
using DataCity.Interfaces;
using DataCity.Model;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels.Feedback
{
    public class DetailViewModel : ViewModelBase
    {
        public CityFeedback Data { get; set; }
        public Stream SelectedImage = null;
        public Portal Portal { get; set; }



        public MyDictionary<string, string> Resources { get; set; }
        private readonly IDataStore _dataStore;
        private Command _getDataCommand;
        /// <summary>
        /// test
        /// </summary>
        /// <param name="items"></param>
        /// <param name="page"></param>

        public DetailViewModel(CityFeedback item, Page page) : base(page)
        {
            Title = item.Name;
            Data = item;

            Portal = JsonConvert.DeserializeObject<Portal>(DataCity.Helpers.Settings.Portal.ToString());

            var languages = JsonConvert.DeserializeObject<Language>(DataCity.Helpers.Settings.Language.ToString());
            try
            {
                Resources = languages.Translation[DataCity.Helpers.Settings.ActualLocale]["messages"];
            }
            catch (Exception ex)
            {
                Resources = languages.Translation["en"]["messages"];
            }
        }

        public Command GetDataCommand
        {
            get
            {
                return _getDataCommand ?? (_getDataCommand = new Command(async () => await ExecuteGetSubmitCommandAsync(), () => !IsBusy));
            }
        }

        public async Task ExecuteGetSubmitCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetDataCommand.ChangeCanExecute();
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    var data = await dataStore.PostKpi(Data, SelectedImage);
                }
                else
                {
                    App.Database.InsertKpiAsync(new KpiItem() { Json = JsonConvert.SerializeObject(Data) });
                }



                await page.DisplayAlert("", Resources["DakujemeZaVasuOdpoved"], Resources["OK"]);
                DataCity.Common.TaskUtils.SetDetailPage(new Views.Feedback.CityFeedback());

            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["OK"]);
                return;
            }
            catch (Exception)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
                return;
            }
            finally
            {
                IsBusy = false;
                GetDataCommand.ChangeCanExecute();
            }
        }
    }
}
