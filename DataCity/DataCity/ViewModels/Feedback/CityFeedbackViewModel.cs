﻿using DataCity.Extensions;
using DataCity.Interfaces;
using DataCity.Model;
using MvvmHelpers;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels.Feedback
{
    public class CityFeedbackViewModel : ViewModelBase
    {
        private readonly IDataStore _dataStore;
        private Command _getDataCommand;
        private CityFeedback _selectedItem;

        public ObservableRangeCollection<CityFeedback> Data { get; set; }

        public Action<CityFeedback> ItemSelected { get; set; }

        public Portal Portal { get; set; }

        public MyDictionary<string, string> Resources { get; set; }

        public CityFeedbackViewModel(Page page) : base(page)
        {
            _dataStore = DependencyService.Get<IDataStore>();
            Data = new ObservableRangeCollection<CityFeedback>();

            Portal = JsonConvert.DeserializeObject<Portal>(DataCity.Helpers.Settings.Portal.ToString());

            var languages = JsonConvert.DeserializeObject<Language>(DataCity.Helpers.Settings.Language.ToString());
            try
            {
                Resources = languages.Translation[DataCity.Helpers.Settings.ActualLocale]["messages"];
            }
            catch (Exception ex)
            {
                Resources = languages.Translation["en"]["messages"];
            }
        }

        public Command GetDataCommand
        {
            get
            {
                return _getDataCommand ?? (_getDataCommand = new Command(async () => await ExecuteGetSubmitCommandAsync(), () => !IsBusy));
            }
        }

        public CityFeedback SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");

                if (_selectedItem == null)
                {
                    return;
                }

                if (ItemSelected == null)
                {
                    switch(_selectedItem.UseCase)
                    {

                        case "dropdown-rating-timestamp":
                            page.Navigation.PushAsync(new Views.Feedback.DropdownRatingTimestamp(_selectedItem));
                            break;
                        case "gps-timestamp-gps-timestamp":
                            page.Navigation.PushAsync(new Views.Feedback.GpsTimestampGpsTimestamp(_selectedItem));
                            break;
                        case "gps-timestamp-timestamp":
                            page.Navigation.PushAsync(new Views.Feedback.GpsTimestampTimestamp(_selectedItem));
                            break;
                        case "gps-image-rating-timestamp":
                            page.Navigation.PushAsync(new Views.Feedback.GpsImageRatingTimestamp(_selectedItem));
                            break;
                        case "gps-image-timestamp":
                            page.Navigation.PushAsync(new Views.Feedback.GpsImageTimestamp(_selectedItem));
                            break;
                        default:
                            page.Navigation.PushAsync(new Views.Feedback.Detail(_selectedItem));
                            break;
                    }
                    
                    SelectedItem = null;
                }
                else
                {
                    ItemSelected.Invoke(_selectedItem);
                }
            }
        }

        public async Task ExecuteGetSubmitCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetDataCommand.ChangeCanExecute();
            var showAlert = false;
            try
            {

                var dataStore = DependencyService.Get<IDataStore>();
                if (CrossConnectivity.Current.IsConnected)
                {
                    var data = await dataStore.GetCityFeedback();
                    Data.ReplaceRange(data);
                }
                else
                {
                    var str = await App.Database.GetItemByNameAsync("GetCityFeedback");
                    var json = JsonConvert.DeserializeObject<List<CityFeedback>>(str.Json);
                    Data.ReplaceRange(json);
                }
            }
            catch (Exception ex)
            {
                showAlert = true;
            }
            finally
            {
                IsBusy = false;
                GetDataCommand.ChangeCanExecute();
            }

            if (showAlert)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
            }
            
        }
    }
}
