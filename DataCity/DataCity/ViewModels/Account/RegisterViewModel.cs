﻿using DataCity.Extensions;
using DataCity.Interfaces;
using DataCity.Model;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels.Account
{
    public class RegisterViewModel : ViewModelBase
    {
        private readonly IDataStore _dataStore;
        private Command _getDataCommand;
        public MyDictionary<string, string> Resources { get; set; }
        public Register Data { get; set; }

        public Portal Portal { get; set; }

        public RegisterViewModel(Page page) : base(page)
        {
            _dataStore = DependencyService.Get<IDataStore>();
            Data = new Register();

            Portal = JsonConvert.DeserializeObject<Portal>(DataCity.Helpers.Settings.Portal.ToString());

            var languages = JsonConvert.DeserializeObject<Language>(DataCity.Helpers.Settings.Language.ToString());
            try
            {
                Resources = languages.Translation[DataCity.Helpers.Settings.ActualLocale]["messages"];
            }
            catch (Exception ex)
            {
                Resources = languages.Translation["en"]["messages"];
            }
        }

        public Command GetSubmitCommand
        {
            get
            {
                return _getDataCommand ?? (_getDataCommand = new Command(async () => await ExecuteGetSubmitCommandAsync(), () => !IsBusy));
            }
        }

        public async Task ExecuteGetSubmitCommandAsync()
        {
            

            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetSubmitCommand.ChangeCanExecute();
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    var result = await dataStore.PostRegistrer(Data);

                    await page.DisplayAlert("", result.Msg, Resources["OK"]);
                    await App.Current.MainPage.Navigation.PushAsync(new Views.Account.Login(), false);
                }
                else
                {
                    await page.DisplayAlert("", Resources["PleaseCheckInternetConnection"], Resources["OK"]);
                }
            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["OK"]);
            }
            catch (Exception ex2)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
            }
            finally
            {
                IsBusy = false;
                GetSubmitCommand.ChangeCanExecute();
            }
        }
    }
}
