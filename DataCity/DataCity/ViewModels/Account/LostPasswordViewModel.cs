﻿using DataCity.Common;
using DataCity.Extensions;
using DataCity.Interfaces;
using DataCity.Model;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels.Account
{
    public class LostPasswordViewModel : ViewModelBase
    {
        private readonly IDataStore _dataStore;
        private Command _resetCommand;
        public MyDictionary<string, string> Resources { get; set; }
        public Login Data { get; set; }
        public LostPasswordViewModel(Page page) : base(page)
        {
            _dataStore = DependencyService.Get<IDataStore>();
            Data = new Login();

            var languages = JsonConvert.DeserializeObject<Language>(DataCity.Helpers.Settings.Language.ToString());
            try
            {
                Resources = languages.Translation[DataCity.Helpers.Settings.ActualLocale]["messages"];
            }
            catch (Exception ex)
            {
                Resources = languages.Translation["en"]["messages"];
            }
        }

        public Command GetResetCommand
        {
            get
            {
                return _resetCommand ?? (_resetCommand = new Command(async () => await ExecuteResetCommandAsync(), () => !IsBusy));
            }
        }

        public async Task ExecuteResetCommandAsync()
        {


            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetResetCommand.ChangeCanExecute();
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    var result = await dataStore.PostResetPassword(new DataCity.Model.Account() { Email = Data.Email });

                    await page.DisplayAlert("", result.Msg, Resources["OK"]);
                }
                else
                {
                    await page.DisplayAlert("", Resources["PleaseCheckInternetConnection"], Resources["OK"]);
                }
            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["OK"]);
            }
            catch (Exception ex2)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
            }
            finally
            {
                IsBusy = false;
                GetResetCommand.ChangeCanExecute();
            }
        }
    }
}
