﻿using DataCity.Interfaces;
using DataCity.Model;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels.Account
{
    public class SelectCityViewModel : ViewModelBase
    {
        private readonly IDataStore _dataStore;
        private Command _getDataCommand;
        private Command _tapCommand;
        public ObservableCollection<Portal> Data { get; set; }

        public SelectCityViewModel(Page page) : base(page)
        {
            _dataStore = DependencyService.Get<IDataStore>();
            Data = new ObservableCollection<Portal>();
            //Application.Current.MainPage = new DataCity.Views.MainPage();
            //TaskUtils.SetDetailPage(new Views.Account.SelectCity());
        }

        public Command TapCommand
        {
            get
            {
                return _tapCommand = _tapCommand ?? new Command(async (x) =>
                {
                    var item = x as Portal;
                    if (item.State == "active")
                    {
                        DataCity.Helpers.Settings.Portal = JsonConvert.SerializeObject(item);

                        var Portal = JsonConvert.DeserializeObject<Portal>(DataCity.Helpers.Settings.Portal.ToString());
                        DataCity.Helpers.Settings.ActualLocale = Portal.Language.Code;

                        await App.Current.MainPage.Navigation.PushAsync(new Views.Account.Login(), false);
                    }

                });
            }
        }

        public Command GetDataCommand
        {
            get
            {
                return _getDataCommand ?? (_getDataCommand = new Command(async () => await ExecuteGetSubmitCommandAsync(), () => !IsBusy));
            }
        }

        public async Task ExecuteGetSubmitCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetDataCommand.ChangeCanExecute();
            var showAlert = false;
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    dataStore.GetLanguages();
                    var data = await dataStore.GetPortals();
                    Data.Clear();
                    foreach (var item in data)
                        Data.Add(item);
                }
                else
                {
                    await page.DisplayAlert("", "Please check internet connection", "Cancel");
                }
            }
            catch (Exception ex)
            {
                showAlert = true;
            }
            finally
            {
                IsBusy = false;
                GetDataCommand.ChangeCanExecute();
            }

            if (showAlert)
            {
                await page.DisplayAlert("", "Something went wrong", "Cancel");
            }

        }
    }
}
