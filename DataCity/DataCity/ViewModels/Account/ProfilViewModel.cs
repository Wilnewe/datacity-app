﻿using DataCity.Extensions;
using DataCity.Interfaces;
using DataCity.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels.Account
{
    public class ProfilViewModel : ViewModelBase
    {
        public IDataStore DataStore { get; }

        private Command _getProfilCommand;
        private Command _deleteProfilCommand;
        private Command _getRewardCommand;
        private Command _postProfilCommand;
        

        public MyDictionary<string, string> Resources { get; set; }
        public Register Data { get; set; }

        public ObservableCollection<RewardPoint> Rewards { get; set; }

        public ProfilViewModel(Page page) : base(page)
        {
            DataStore = DependencyService.Get<IDataStore>();
            Rewards = new ObservableCollection<RewardPoint>();
            Data = new Register()
            {
                FirstName = "messages"

            };

            var languages = JsonConvert.DeserializeObject<Language>(Helpers.Settings.Language);
            try
            {
                Resources = languages.Translation[Helpers.Settings.ActualLocale]["messages"];
            }
            catch (Exception)
            {
                Resources = languages.Translation["en"]["messages"];
            }

        }

        public Command GetProfilCommand
        {
            get
            {
                return _getProfilCommand ?? (_getProfilCommand = new Command(async () => await ExecuteGetProfilCommandAsync(), () => !IsBusy));
            }
        }

        public Command PostProfilCommand
        {
            get
            {
                return _postProfilCommand ?? (_postProfilCommand = new Command(async () => await ExecutePostProfilCommandAsync(), () => !IsBusy));
            }
        }

        public Command DeleteProfilCommand
        {
            get
            {
                return _deleteProfilCommand ?? (_deleteProfilCommand = new Command(async () => await ExecuteDeleteProfilCommandAsync(), () => !IsBusy));
            }
        }

        public Command GetRewardCommand
        {
            get
            {
                return _getRewardCommand ?? (_getRewardCommand = new Command(async () => await ExecuteGetRewardCommandAsync(), () => !IsBusy));
            }
        }

        public async Task ExecuteGetProfilCommandAsync()
        {
            

            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetProfilCommand.ChangeCanExecute();
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    var data = await dataStore.GetProfil();

                    Data.FirstName = data.FirstName;
                    Data.LastName = data.LastName;
                    Data.Email = data.Email;
                    Data.Gender = data.Gender;
                    Data.Phone = data.Phone;
                    Data.BirthDate = data.BirthDate;
                    Data.RewardPointsActual = data.RewardPointsActual;
                    Data.RewardPointsEarned = data.RewardPointsEarned;
                    Data.RewardPointsReceived = data.RewardPointsReceived;
                    //data.CopyPrope.rties(Data);
                }
                else
                {
                    await page.DisplayAlert("", Resources["PleaseCheckInternetConnection"], Resources["OK"]);
                }
            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["OK"]);
            }
            catch (Exception)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
            }
            finally
            {
                IsBusy = false;
                GetProfilCommand.ChangeCanExecute();
            }
        }

        public async Task ExecutePostProfilCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            PostProfilCommand.ChangeCanExecute();
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    var result = await dataStore.PostProfil(Data);

                    await page.DisplayAlert("", result.Msg, Resources["OK"]);
 }
                else
                {
                    await page.DisplayAlert("", Resources["PleaseCheckInternetConnection"], Resources["OK"]);
                }
            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["OK"]);
            }
            catch (Exception ex2)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
            }
            finally
            {
                IsBusy = false;
                PostProfilCommand.ChangeCanExecute();
            }
        }

        public async Task ExecuteDeleteProfilCommandAsync()
        {


            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            DeleteProfilCommand.ChangeCanExecute();
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    var result = await dataStore.DeleteProfil();

                    await page.DisplayAlert("", result.Msg, Resources["OK"]);
                }
                else
                {
                    await page.DisplayAlert("", Resources["PleaseCheckInternetConnection"], Resources["OK"]);
                }
            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["OK"]);
            }
            catch (Exception)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
            }
            finally
            {
                IsBusy = false;
                DeleteProfilCommand.ChangeCanExecute();
            }
        }

        public async Task ExecuteGetRewardCommandAsync()
        {


            //if (IsBusy)
            //{
            //    return;
            //}

            //IsBusy = true;
            GetRewardCommand.ChangeCanExecute();
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    var data = await dataStore.GetRewardPoints();

                    Rewards.Clear();
                    foreach (var item in data)
                        Rewards.Add(item);
                }
                else
                {
                    await page.DisplayAlert("", Resources["PleaseCheckInternetConnection"], Resources["OK"]);
                }
            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["OK"]);
            }
            catch (Exception)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
            }
            finally
            {
               // IsBusy = false;
                GetRewardCommand.ChangeCanExecute();
            }
        }

    }
}
