﻿using DataCity.Common;
using DataCity.Extensions;
using DataCity.Interfaces;
using DataCity.Model;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels.Account
{
    public class LoginViewModel : ViewModelBase
    {
        private readonly IDataStore _dataStore;
        private Command _getDataCommand;
        public MyDictionary<string, string> Resources { get; set; }
        public Login Data { get; set; }
        public LoginViewModel(Page page) : base(page)
        {
            _dataStore = DependencyService.Get<IDataStore>();
            Data = new Login();

            var languages = JsonConvert.DeserializeObject<Language>(DataCity.Helpers.Settings.Language.ToString());
            try
            {
                Resources = languages.Translation[DataCity.Helpers.Settings.ActualLocale]["messages"];
            }
            catch (Exception ex)
            {
                Resources = languages.Translation["en"]["messages"];
            }
        }

        public Command GetSubmitCommand
        {
            get
            {
                return _getDataCommand ?? (_getDataCommand = new Command(async () => await ExecuteGetSubmitCommandAsync(), () => !IsBusy));
            }
        }

        public async Task ExecuteGetSubmitCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetSubmitCommand.ChangeCanExecute();
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    var result = await dataStore.PostLogin(Data);

                    Application.Current.MainPage = new DataCity.Views.MainPage();
                    TaskUtils.SetDetailPage(new Views.Feedback.CityFeedback());
                }
                else
                {
                    await page.DisplayAlert("", Resources["PleaseCheckInternetConnection"], Resources["OK"]);
                }
            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["OK"]);
                return;
            }
            catch (Exception)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
                return;
            }
            finally
            {
                IsBusy = false;
                GetSubmitCommand.ChangeCanExecute();
            }

            
        }
    }
}
