﻿using DataCity.Extensions;
using DataCity.Interfaces;
using DataCity.Model;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels.Account
{
    public class RewardsViewModel : ViewModelBase
    {
        public IDataStore DataStore { get; }

        private Command _getRewardCommand;

        public MyDictionary<string, string> Resources { get; set; }


        public ObservableCollection<RewardPoint> Data { get; set; }

        public RewardsViewModel(Page page) : base(page)
        {
            DataStore = DependencyService.Get<IDataStore>();
            Data = new ObservableCollection<RewardPoint>();
            var languages = JsonConvert.DeserializeObject<Language>(Helpers.Settings.Language);
            try
            {
                Resources = languages.Translation[Helpers.Settings.ActualLocale]["messages"];
            }
            catch (Exception)
            {
                Resources = languages.Translation["en"]["messages"];
            }

        }


        public Command GetRewardCommand
        {
            get
            {
                return _getRewardCommand ?? (_getRewardCommand = new Command(async () => await ExecuteGetRewardCommandAsync(), () => !IsBusy));
            }
        }

        public async Task ExecuteGetRewardCommandAsync()
        {


            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetRewardCommand.ChangeCanExecute();
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    var data = await dataStore.GetRewardPoints();

                    Data.Clear();
                    foreach (var item in data)
                        Data.Add(item);
                }
                else
                {
                    await page.DisplayAlert("", Resources["PleaseCheckInternetConnection"], Resources["OK"]);
                }
            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["OK"]);
            }
            catch (Exception)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
            }
            finally
            {
                IsBusy = false;
                GetRewardCommand.ChangeCanExecute();
            }
        }

    }
}
