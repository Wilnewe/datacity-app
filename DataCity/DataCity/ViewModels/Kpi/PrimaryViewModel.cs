﻿using DataCity.Extensions;
using DataCity.Interfaces;
using DataCity.Model;
using MvvmHelpers;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels.Kpi
{
    public class PrimaryViewModel : ViewModelBase
    {
        private readonly IDataStore _dataStore;
        private Command _getDataCommand;
        private CityFeedback _selectedItem;

        public ObservableRangeCollection<CityFeedback> Data { get; set; }
        public MyDictionary<string, string> Resources { get; set; }

        public PrimaryViewModel(Page page) : base(page)
        {
            _dataStore = DependencyService.Get<IDataStore>();
            Data = new ObservableRangeCollection<CityFeedback>();

            var languages = JsonConvert.DeserializeObject<Language>(DataCity.Helpers.Settings.Language.ToString());
            try
            {
                Resources = languages.Translation[DataCity.Helpers.Settings.ActualLocale]["messages"];
            }
            catch (Exception ex)
            {
                Resources = languages.Translation["en"]["messages"];
            }
        }

        public void Navigate(CityFeedback _selectedItem)
        {

            switch (_selectedItem.UseCase)
            {

                case "dropdown-rating-timestamp":
                    page.Navigation.PushAsync(new Views.Feedback.DropdownRatingTimestamp(_selectedItem));
                    break;
                case "gps-timestamp-gps-timestamp":
                    page.Navigation.PushAsync(new Views.Feedback.GpsTimestampGpsTimestamp(_selectedItem));
                    break;
                case "gps-timestamp-timestamp":
                    page.Navigation.PushAsync(new Views.Feedback.GpsTimestampTimestamp(_selectedItem));
                    break;
                case "gps-image-rating-timestamp":
                    page.Navigation.PushAsync(new Views.Feedback.GpsImageRatingTimestamp(_selectedItem));
                    break;
                case "gps-image-timestamp":
                    page.Navigation.PushAsync(new Views.Feedback.GpsImageTimestamp(_selectedItem));
                    break;
                default:
                    page.Navigation.PushAsync(new Views.Feedback.Detail(_selectedItem));
                    break;
            }

        }

        public Command GetDataCommand
        {
            get
            {
                return _getDataCommand ?? (_getDataCommand = new Command(async () => await ExecuteGetSubmitCommandAsync(), () => !IsBusy));
            }
        }

        public async Task ExecuteGetSubmitCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetDataCommand.ChangeCanExecute();
            var showAlert = false;
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var dataStore = DependencyService.Get<IDataStore>();
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        var data = await dataStore.GetCityFeedback();
                        Data.ReplaceRange(data);
                    }
                    else
                    {
                        var str = await App.Database.GetItemByNameAsync("GetCityFeedback");
                        var json = JsonConvert.DeserializeObject<List<CityFeedback>>(str.Json);
                        Data.ReplaceRange(json);
                    }
                }
                else
                {
                    await page.DisplayAlert("", Resources["PleaseCheckInternetConnection"], Resources["OK"]);
                }
            }
            catch (Exception ex)
            {
                showAlert = true;
            }
            finally
            {
                IsBusy = false;
                GetDataCommand.ChangeCanExecute();
            }

            if (showAlert)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
            }

        }
    }
}
