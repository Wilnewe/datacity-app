﻿using DataCity.Extensions;
using DataCity.Interfaces;
using DataCity.Model;
using MvvmHelpers;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels.Pool
{
    public class ComunityPoolViewViewModel : ViewModelBase
    {
        private readonly IDataStore _dataStore;
        private Command _getDataCommand;
        private Command _postDataCommand;

        public int RespondId;
        public int QuestionId;
        public ObservableRangeCollection<CommunityPool> Data { get; set; }
        public MyDictionary<string, string> Resources { get; set; }
        public ComunityPoolViewViewModel(Page page) : base(page)
        {
            _dataStore = DependencyService.Get<IDataStore>();
            Data = new ObservableRangeCollection<CommunityPool>();

            Portal = JsonConvert.DeserializeObject<Portal>(DataCity.Helpers.Settings.Portal.ToString());

            var languages = JsonConvert.DeserializeObject<Language>(DataCity.Helpers.Settings.Language.ToString());
            try
            {
                Resources = languages.Translation[DataCity.Helpers.Settings.ActualLocale]["messages"];
            }
            catch (Exception ex)
            {
                Resources = languages.Translation["en"]["messages"];
            }
        }


        public Command GetDataCommand
        {
            get
            {
                return _getDataCommand ?? (_getDataCommand = new Command(async () => await ExecuteGetSubmitCommandAsync(), () => !IsBusy));
            }
        }

        public Command PostDataCommand
        {
            get
            {
                return _postDataCommand ?? (_postDataCommand = new Command(async () => await ExecutePostSubmitCommandAsync(), () => !IsBusy));
            }
        }

        public async Task ExecuteGetSubmitCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetDataCommand.ChangeCanExecute();
            try
            {
                var dataStore = DependencyService.Get<IDataStore>();
                if (CrossConnectivity.Current.IsConnected)
                {
                    var data = await dataStore.GetComunityPool();
                    Data.ReplaceRange(data);
                }
                else
                {
                    var str = await App.Database.GetItemByNameAsync("GetComunityPool");
                    var json = JsonConvert.DeserializeObject<List<CommunityPool>>(str.Json);
                    Data.ReplaceRange(json);
                }



            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["Ok"]);
                return;
            }
            catch (Exception)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
                return;
            }
            finally
            {
                IsBusy = false;
                GetDataCommand.ChangeCanExecute();
            }
        }


        public async Task ExecutePostSubmitCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetDataCommand.ChangeCanExecute();
            try
            {
                var dataStore = DependencyService.Get<IDataStore>();
                var data = await dataStore.PostComunityPool(QuestionId, RespondId);
                await page.DisplayAlert("", Resources["DakujemeZaVasuOdpoved"], Resources["OK"]);
            }
            catch (OperationCanceledException ex)
            {
                await page.DisplayAlert("", ex.Message, Resources["Ok"]);
                return;
            }
            catch (Exception)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
                return;
            }
            finally
            {
                IsBusy = false;
                GetDataCommand.ChangeCanExecute();
            }
        }
    }
}
