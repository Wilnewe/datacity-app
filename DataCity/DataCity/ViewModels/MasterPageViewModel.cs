﻿using DataCity.Extensions;
using DataCity.Interfaces;
using DataCity.Model;
using MvvmHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity.ViewModels
{

    public class MasterPageViewModel : ViewModelBase
    {
        private readonly IDataStore _dataStore;
        private Command _getDataCommand;

        public ObservableRangeCollection<MasterPageItem> Data { get; set; }

        public Portal Portal { get; set; }

        public MyDictionary<string, string> Resources { get; set; }

        public MasterPageViewModel(Page page) : base(page)
        {
            _dataStore = DependencyService.Get<IDataStore>();
            Data = new ObservableRangeCollection<MasterPageItem>();
            Portal = JsonConvert.DeserializeObject<Portal>(DataCity.Helpers.Settings.Portal.ToString());

            var languages = JsonConvert.DeserializeObject<Language>(DataCity.Helpers.Settings.Language.ToString());
            try
            {
                Resources = languages.Translation[DataCity.Helpers.Settings.ActualLocale]["messages"];
            }
            catch (Exception ex)
            {
                Resources = languages.Translation["en"]["messages"];
            }
        }

        public Command GetDataCommand
        {
            get
            {
                return _getDataCommand ?? (_getDataCommand = new Command(async () => await ExecuteGetSubmitCommandAsync(), () => !IsBusy));
            }
        }

        public async Task ExecuteGetSubmitCommandAsync()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;
            GetDataCommand.ChangeCanExecute();
            var showAlert = false;
            try
            {
                var dataStore = DependencyService.Get<IDataStore>();
                dataStore.GetLanguages();

                //Uz sa pouziva webview
                //var dataStore = DependencyService.Get<IDataStore>();
                //var data = await dataStore.GetKpi();
                //Data.ReplaceRange(data);


                Data.Insert(0, new MasterPageItem
                {
                    Title = Resources["DajVedietMestu"],
                    TargetType = typeof(Views.Feedback.CityFeedback)
                });
                Data.Insert(1, new MasterPageItem
                {
                    Title = Resources["Ankety"],
                    TargetType = typeof(Views.Pool.ComunityPolls),
                    NeedInternet = false
                });
                Data.Insert(1, new MasterPageItem
                {
                    Title = Resources["Dashboard"],
                    TargetType = typeof(Views.Kpi.Primary),
                    NeedInternet = false
                });

                Data.Add(new MasterPageItem
                {
                    Title = Resources["Profil"],
                    TargetType = typeof(Views.Account.Profil),
                    NeedInternet = false
                });

            }
            catch (Exception ex)
            {
                showAlert = true;
            }
            finally
            {
                IsBusy = false;
                GetDataCommand.ChangeCanExecute();
            }

            if (showAlert)
            {
                await page.DisplayAlert("", Resources["NastalaChybaVSpracovaniUdajov"], Resources["OK"]);
            }
        }
    }
}
