// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace DataCity.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string SettingsDefault = string.Empty;

        #endregion


        public static string GeneralSettings
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>(SettingsKey, SettingsDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>(SettingsKey, value);
            }
        }

        public static string UriKpi
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>("UriKpi", "");
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>("UriKpi", value);
            }
        }


        public static string Profil
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>("Profil", "");
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>("Profil", value);
            }
        }

        public static string Portal
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>("Portal", "");
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>("Portal", value);
            }
        }


        public static string Language
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>("Language", "");
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>("Language", value);
            }
        }

        public static string ActualLocale
        {
            get
            {
                return AppSettings.GetValueOrDefault<string>("Locale", "");
            }
            set
            {
                AppSettings.AddOrUpdateValue<string>("Locale", value);
            }
        }

    }
}