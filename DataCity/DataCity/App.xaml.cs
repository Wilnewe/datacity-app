﻿using DataCity.Common;
using DataCity.Data;
using DataCity.Interfaces;
using DataCity.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DataCity
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            Account account = null;

            try
            {
                account = JsonConvert.DeserializeObject<Account>(JObject.Parse(DataCity.Helpers.Settings.Profil).SelectToken("data").ToString());
            }
            catch (Exception ex)
            {
            }

            if (account == null)
            {
                MainPage = new NavigationPage(new DataCity.Views.Account.SelectCity());
                MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
            }
            else
            {
                MainPage = new DataCity.Views.MainPage();
                MainPage.SetValue(NavigationPage.BarTextColorProperty, Color.White);
            }

            //sync
            if (CrossConnectivity.Current.IsConnected)
            {
                
                var kpis = App.Database.GetKpiAsync();
                foreach (var item in kpis.Result)
                {
                    try {
                        var dataStore = DependencyService.Get<IDataStore>();
                        dataStore.PostKpi(JsonConvert.DeserializeObject<CityFeedback>(item.Json), null);
                        App.Database.DeleteKpiAsync(item.ID);

                        dataStore.GetComunityPool();
                        dataStore.GetKpi();
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }
        }

        public string KpiLink = "";


        static Database database;
        public static Database Database
        {
            get
            {
                if (database == null)
                {
                    database = new Database(DependencyService.Get<IFileHelper>().GetLocalFilePath("DataCitySQLite.db3"));
                }
                return database;
            }
        }

        protected override void OnStart()
        {
            /*
            var task = PclStorage.ReadAccountFromFile();
            Model.Account result = null;
            Task continuation = task.ContinueWith(t =>
            {
                result = t.Result;
            });
            continuation.Wait();*/

            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
