﻿using DataCity.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataCity.Data
{
    public class Database
    {
        readonly SQLiteAsyncConnection database;

        public Database(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<StorageItem>().Wait();
            database.CreateTableAsync<KpiItem>().Wait();
        }

        public Task<List<StorageItem>> GetItemsAsync()
        {
            return database.Table<StorageItem>().ToListAsync();
        }

        public Task<List<StorageItem>> GetItemsNotDoneAsync()
        {
            return database.QueryAsync<StorageItem>("SELECT * FROM [StorageItem] WHERE [Done] = 0");
        }

        public Task<StorageItem> GetItemAsync(int id)
        {
            return database.Table<StorageItem>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(StorageItem item)
        {
            if (item.ID != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public async void SaveOrUpdateAsync(StorageItem item)
        {
            List<StorageItem> list = await GetItemsAsync();
            var selectedItem = list.FirstOrDefault(o => o.Name == item.Name);
            if (selectedItem != null)
            {
                await database.UpdateAsync(item);

            }
            else
            {
                await database.InsertAsync(item);
            }
        }



        public async void InsertKpiAsync(KpiItem item)
        {
            await database.InsertAsync(item);
        }

        public Task<List<KpiItem>> GetKpiAsync()
        {
            return database.Table<KpiItem>().ToListAsync();
        }

        public async void DeleteKpiAsync(int id)
        {
            var x = await database.QueryAsync<KpiItem>("DELETE FROM [KpiItem] WHERE [ID] = " + id);
            var c = 0;
        }


        public async Task<StorageItem> GetItemByNameAsync(string name)
        {
            List<StorageItem> list = await GetItemsAsync();
            return list.FirstOrDefault(o => o.Name == name);
        }

        public Task<int> DeleteItemAsync(StorageItem item)
        {
            return database.DeleteAsync(item);
        }
    }
}
